<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {            
            $user = null;
            
            $isAdmin = false;
            
            if(\Session::get('sess') !== null) {
                $user = DB::table('users')->where(['id' => DB::table('sessions')->where(['id' => \Session::get('sess')])->value('user')])->first();
                
                if($user !== null) {
                    if($user->access === 'board' || $user->access === 'administrator')
                        $isAdmin = true;
                }
            }
            
            $view->with(['isAdmin' => $isAdmin]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
