<?php
/**
* ReportController
*
* Creates reports once meetings are published
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use HTML2PDF;

class ReportController extends Controller {
    public function render(Request $request, $meetingID = null) {
        if(parent::user() === null)
            return redirect('/login');
        
        return self::renderReport($request, $meetingID);
    }
    
    public function pdf(Request $request, $meetingID) {
        if(parent::user() === null)
            return redirect('/login');
        
        return self::renderReport($request, $meetingID, true);
    }
    
    public function renderReport(Request $request, $meetingID, $pdf = false) {
        $meeting = Admin\MeetingController::helperMeetingGet($meetingID);
        
        if(isset($meeting) && (($meeting->type === 'GA' && ($meeting->status === 'closed' || $meeting->status === 'published')) || ($meeting->type === 'BoG' && ((parent::user()->access === 'board' && $meeting->status === 'closed') || $meeting->status === 'published')))) {
            $users = array();
            
            $count_invited = 0;
            $count_present = 0;
            $count_proxied = 0;
            
            if($meeting->present !== null) {
                $meeting_present = json_decode($meeting->present, true);
                
                if(count($meeting_present) > 0) {
                    foreach($meeting_present AS $id => $present) {
                        $count_invited++;
                        
                        if($present['status'] === 'present')
                            $count_present++;
                        
                        if($present['status'] === 'proxied')
                            $count_proxied++;
                        
                        $users[] = ['id' => $id, 'name' => $present['name'], 'status' => $present['status'], 'proxy' => (array_key_exists('proxy', $present) ? $present['proxy'] : null)];
                    }
                }
            }
                        
            $count_absent = $count_invited - $count_present - $count_proxied;
            
            /* Topics */
            $topics = json_decode($meeting->topics);
            
            /* Motions */
            $motions = array();
            
            if(count($topics) > 0) {
                foreach($topics AS $topic) {
                    if(isset($topic->motions) && count($topic->motions) > 0) {
                        foreach($topic->motions AS $topic_motion) {
                            $result = null;

                            if($topic_motion->result !== null) {
                                $result = json_decode(json_encode($topic_motion->result), true);
                                
                                if($result !== null && array_key_exists('votes', $result) && count($result['votes']) > 0) {
                                    $agree_votes = array();
                                    $disagree_votes = array();
                                    $abstain_votes = array();

                                    foreach($result['votes'] AS $vote) {
                                        if($vote['vote'] === 'agree')
                                            $agree_votes[] = $vote['name'].' ('.$vote['user'].')';
                                        elseif($vote['vote'] === 'disagree')
                                            $disagree_votes[] = $vote['name'].' ('.$vote['user'].')';
                                        elseif($vote['vote'] === 'abstain')
                                            $abstain_votes[] = $vote['name'].' ('.$vote['user'].')';
                                    }

                                    $result['agree_votes'] = $agree_votes;
                                    $result['disagree_votes'] = $disagree_votes;
                                    $result['abstain_votes'] = $abstain_votes;
                                }
                            }
                            
                            $motions[$topic->id][] = array('motion' => $topic_motion, 'result' => $result);
                        }
                    }
                }
            }
            
            /* Meeting status */
            if(round($count_absent / $count_invited * 100) <= 33)
                $meeting_status = 'valid for statutes';
            else
                $meeting_status = 'valid except statutes';
            
            if($pdf === true) {
                $html2pdf = new HTML2PDF('P', 'A4', 'nl');
				$html2pdf->setTestTdInOnePage(false);
                $html2pdf->WriteHTML(View('pdf', ['meeting' => $meeting, 'topics' => $topics, 'motions' => $motions, 'users' => $users, 'count_invited' => $count_invited, 'count_present' => $count_present.' ('.round($count_present / $count_invited * 100).'%)', 'count_proxied' => $count_proxied.' ('.round($count_proxied / $count_invited * 100).'%)', 'count_absent' => $count_absent.' ('.round($count_absent / $count_invited * 100).'%)', 'meeting_status' => $meeting_status]));

                return $html2pdf->Output('Meeting Report #'.$meeting->id.'.pdf');
            } else {
                return View('report', ['meeting' => $meeting, 'topics' => $topics, 'motions' => $motions, 'users' => $users, 'count_invited' => $count_invited, 'count_present' => $count_present.' ('.round($count_present / $count_invited * 100).'%)', 'count_proxied' => $count_proxied.' ('.round($count_proxied / $count_invited * 100).'%)', 'count_absent' => $count_absent.' ('.round($count_absent / $count_invited * 100).'%)', 'meeting_status' => $meeting_status]);
            }
        }
    }
}