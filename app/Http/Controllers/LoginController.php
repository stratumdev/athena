<?php
/**
* LoginController
*
* Redirect to the IVAO Login API and handles the return from the API
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LoginController extends Controller {
    public function render(Request $request) {
        if($request->get('IVAOTOKEN') !== null) {
            if($request->get('IVAOTOKEN') === 'error')
                exit('Login API error, please try again later or contact pim.oude.veldhuis@ivao.aero');
            
            $jsonstr = file_get_contents('http://login.ivao.aero/api.php?type=json&token='.$request->get('IVAOTOKEN'));
            if($jsonstr !== '') {
                $userinfo = json_decode($jsonstr, true);
                if(!array_key_exists('vid', $userinfo) || !array_key_exists('isNpoMember', $userinfo))
                    exit('Login API error, cannot access NPO information, please contact pim.oude.veldhuis@ivao.aero');
                
                $user = DB::table('users')->where('id', $userinfo['vid'])->first();
                
                $access = 'adjoined';
                if($userinfo['isNpoMember'] == '1')
                    $access = 'member';

                if($user === null) {
                    DB::table('users')->insert(['id' => $userinfo['vid'], 'status' => 'inactive', 'access' => $access, 'name' => $userinfo['firstname'].' '.$userinfo['lastname']]);
                    $user = DB::table('users')->where('id', $userinfo['vid'])->first();
                }
                
                if($user !== null) {
                    if($user->access === 'board') $access = 'board';
                    if($user->access === 'administrator') $access = 'administrator';
                    DB::table('users')->where('id', $userinfo['vid'])->update(['access' => $access, 'name' => $userinfo['firstname'].' '.$userinfo['lastname']]);

                    DB::table('sessions')->insert(['user' => $user->id, 'datetime' => new DateTime(), 'iphash' => sha1($_SERVER['REMOTE_ADDR'])]);
                    $request->session()->put('sess', DB::getPdo()->lastInsertId());
                    
                    return redirect('/');
                } else { exit('Login API error, please try again later or contact pim.oude.veldhuis@ivao.aero'); }
            }
        } else {
            return redirect('http://login.ivao.aero/index.php?url='.env('APP_URL').'/login');
        }
    }
    
    public function logout(Request $request) {
        $request->session()->forget('sess');
        return redirect('/');
    }
}