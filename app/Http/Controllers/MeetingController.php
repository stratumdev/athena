<?php
/**
* MeetingController
*
* Meeting manager, also handles the counting of members etc.
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MeetingController extends Controller {
    public function render(Request $request, $meetingID = null, $topicID = null, $motionID = null) {
        if(parent::user() === null || parent::user()->status !== 'active' || (parent::user()->access !== 'member' && parent::user()->access !== 'board' && parent::user()->access !== 'administrator'))
            return redirect('/login');
        
        if($motionID !== null) {
            return self::renderMotion($request, $meetingID, $topicID, $motionID);
        } elseif($topicID !== null) {
            return self::renderTopic($request, $meetingID, $topicID);
        } elseif($meetingID !== null) {
            return self::renderMeeting($request, $meetingID);
        }
    }
    
    public function renderMeeting(Request $request, $meetingID) {
        $meeting = Admin\MeetingController::helperMeetingGet($meetingID);
        
        if(isset($meeting) && ($meeting->type === 'GA' || ($meeting->type === 'BoG' && parent::user()->access === 'board'))) {
            self::helperUserAttending($meeting);
            
            /* Users */
            $users = array();
            
            $users_invited = DB::table('users')->where('status', 'active')->get();
            
            $count_invited = 0;
            $count_present = 0;
            $count_proxied = 0;
            
            if(count($users_invited) > 0) {
                foreach($users_invited AS $user_invited) {
                    $users[$user_invited->id] = ['id' => $user_invited->id, 'name' => $user_invited->name, 'status' => 'invited', 'proxy' => null];
                    $count_invited++;
                }
            }
            
            $meeting_proxies = DB::table('meeting_proxies')->where('meeting', $meeting->id)->get();
            if(count($meeting_proxies) > 0) {
                foreach($meeting_proxies AS $meeting_proxy) {
                    $proxy = DB::table('users')->where('id', $meeting_proxy->proxy)->first();
                    if($proxy !== null) {
                        $users[$meeting_proxy->user]['status'] = 'proxy-invited';
                        $users[$meeting_proxy->user]['proxy'] = $proxy->id.' - '.$proxy->name;
                    }
                }
            }
            
            $meeting_users = DB::table('meeting_users')->where('meeting', $meeting->id)->get();
            if(count($meeting_users) > 0) {
                foreach($meeting_users AS $meeting_user) {
                    $users[$meeting_user->user]['status'] = 'present';
                    $count_present++;

                    if($meeting_user->proxy !== null) {
                        $users[$meeting_user->proxy]['status'] = 'proxied';
                        $users[$meeting_user->proxy]['proxy'] = $meeting_user->user.' - '.$users[$meeting_user->user]['name'];
                        $count_proxied++;
                    }
                }
            }
                        
            $count_absent = $count_invited - $count_present - $count_proxied;
            
            /* Topics */
            $topics = DB::table('topics')->where('meeting', $meeting->id)->orderBy('sort', 'ASC')->get();
            
            /* Meeting status */
            if(round($count_absent / $count_invited * 100) <= 33)
                $meeting_status = 'valid for statutes';
            else
                $meeting_status = 'valid except statutes';
            
            return View('meeting', ['meeting' => $meeting, 'users' => $users, 'count_invited' => $count_invited, 'count_present' => $count_present.' ('.round($count_present / $count_invited * 100).'%)', 'count_proxied' => $count_proxied.' ('.round($count_proxied / $count_invited * 100).'%)', 'count_absent' => $count_absent.' ('.round($count_absent / $count_invited * 100).'%)', 'meeting_status' => $meeting_status, 'topics' => $topics]);
        }
    }
    
    public function renderTopic(Request $request, $meetingID, $topicID) {
        $meeting = Admin\MeetingController::helperMeetingGet($meetingID);
		$user = DB::table('meeting_users')->where([['meeting', '=', $meeting->id], ['user', '=', parent::user()->id]])->first();
        if(isset($meeting) && ($meeting->type === 'GA' || ($meeting->type === 'BoG' && parent::user()->access === 'board'))) {
            self::helperUserAttending($meeting);

            $topic = DB::table('topics')->where('id', $topicID)->first();
            if(isset($topic)) {
                $motions = DB::table('motions')->where('topic', $topicID)->get();
                $motions_rendered = array();
                
                if(count($motions) > 0) {
                    foreach($motions AS $motion) {
						$waiting_for = null;
						$result = null;
                        if($motion->result !== null) {
                            $motion->result = json_decode($motion->result);
                            $outcome = $motion->result->outcome;
							
							if(isset($motion->result->votes) && count($motion->result->votes) > 0) {
                				$agree_votes = array();
                				$disagree_votes = array();
                				$abstain_votes = array();
                
                				foreach($motion->result->votes AS $vote) {
                    				if($vote->vote === 'agree')
                        				$agree_votes[] = $vote->name.' ('.$vote->user.')';
                    				elseif($vote->vote === 'disagree')
                        				$disagree_votes[] = $vote->name.' ('.$vote->user.')';
                    				elseif($vote->vote === 'abstain')
                        				$abstain_votes[] = $vote->name.' ('.$vote->user.')';
                				}
                
                				$result['agree_votes'] = $agree_votes;
                				$result['disagree_votes'] = $disagree_votes;
                				$result['abstain_votes'] = $abstain_votes;
            				}
                        }
						DB::enableQueryLog();
						
						$waitingfor = DB::table('meeting_users')
							->where([['meeting', '=', $meeting->id]])
							->where(function($query) use ($motion) {
								$query->whereNotIn('user', DB::table('log_votes')->where('motion', $motion->id)->pluck('user'))
        						->orWhereIn('user', DB::table('meeting_users')->whereNotIn('proxy', DB::table('log_votes')->where('motion', $motion->id)->pluck('user'))->pluck('user'));
    						})
    						->get();
							
            			if(count($waitingfor) > 0)
                			$waiting_for = count($waitingfor);
                                                
                        $did_i_vote = DB::table('log_votes')->where([['motion', '=', $motion->id], ['user', '=', parent::user()->id]])->first();
						
						$proxy = null;
                		if($user->proxy !== null){
							$proxy = DB::table('users')->where('id', $user->proxy)->first();
							$need_i_vote_proxy = (DB::table('log_votes')->where([['motion', '=', $motion->id], ['user', '=', $proxy->id]])->count() > 0 ? false : true);
						} else {
							$need_i_vote_proxy = false;
						}
							
                        $motions_rendered[] = ['id' => $motion->id, 'status' => $motion->status, 'description' => $motion->description, 'outcome' => (isset($outcome) ? $outcome : null), 'did_i_vote' => (($did_i_vote !== null) ? true : false), 'need_i_vote_proxy' => $need_i_vote_proxy, 'result' => $result, 'waiting_for' => $waiting_for, 'proxy' => $proxy];
                    }
                }

                return View('topic', ['meeting' => $meeting, 'topic' => $topic, 'motions' => $motions_rendered]);
            }
        }
    }
	
	public function renderMotions(Request $request, $meetingID, $topicID){
		$meeting = Admin\MeetingController::helperMeetingGet($meetingID);
		$topic = DB::table('topics')->where('id', $topicID)->first();
		$user = DB::table('meeting_users')->where([['meeting', '=', $meeting->id], ['user', '=', parent::user()->id]])->first();
		
		if(is_array($request->input('self'))){
			$votes_self = $request->input('self');
			foreach($votes_self as $motion_id => $vote){
				$motion = Admin\MotionController::helperMotionGet($motion_id);
				$voting_log = DB::table('log_votes')->where([['motion', '=', $motion->id], ['user', '=', $user->user]])->first();
                if($voting_log === null) {
                    if($motion->type === 'public') {
                        DB::beginTransaction();
                        DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->user]);
                        DB::table('votes')->insert(['motion' => $motion->id, 'user' => $user->user, 'vote' => $vote]);
                        DB::commit();
                    } else {
                        DB::beginTransaction();
                        DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->user]);
                        DB::table('votes')->insert(['motion' => $motion->id, 'vote' => $vote]);
                        DB::commit();
                    }
                }
			}
		}
		
		if(is_array($request->input('proxy'))){
			$votes_proxy = $request->input('proxy');
			foreach($votes_proxy as $motion_id => $vote){
				$motion = Admin\MotionController::helperMotionGet($motion_id);
				$voting_log_proxy = DB::table('log_votes')->where([['motion', '=', $motion->id], ['user', '=', $user->proxy]])->first();
                if($voting_log_proxy === null) {
                    if($motion->type === 'public') {
                        DB::beginTransaction();
                        DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->proxy]);
                        DB::table('votes')->insert(['motion' => $motion->id, 'user' => $user->proxy, 'vote' => $vote]);
                        DB::commit();
                    } else {
                        DB::beginTransaction();
                        DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->proxy]);
                        DB::table('votes')->insert(['motion' => $motion->id, 'vote' => $vote]);
                        DB::commit();
                     }
                 }
			}
		}
		
		return redirect('/meeting/'. $meeting->id . '/topic/' . $topic->id);
		
	}
	
    public function renderMotion(Request $request, $meetingID, $topicID, $motionID) {
        $meeting = Admin\MeetingController::helperMeetingGet($meetingID);
        if(isset($meeting) && ($meeting->type === 'GA' || ($meeting->type === 'BoG' && parent::user()->access === 'board'))) {
            self::helperUserAttending($meeting);

            $topic = DB::table('topics')->where('id', $topicID)->first();
            $motion = Admin\MotionController::helperMotionGet($motionID);

            $user = DB::table('meeting_users')->where([['meeting', '=', $meeting->id], ['user', '=', parent::user()->id]])->first();

            if(isset($topic) && isset($motion) && isset($user)) {
                $proxy = null;
                if($user->proxy !== null)
                    $proxy = DB::table('users')->where('id', $user->proxy)->first();

                if($request->method() === 'POST') {
                    $error = array();
                    
                    if($request->input('self') === null)
                        $error[] = 'Please choose what to vote before submitting your vote';
                    
                    if($proxy !== null && $request->input('proxy') === null)
                        $error[] = 'Please choose what to vote for your proxy before submitting the vote';
                    
                    if(count($error) > 0) {
                        return View('motion', ['meeting' => $meeting, 'topic' => $topic, 'motion' => $motion, 'error' => $error, 'user' => $user, 'proxy' => $proxy, 'input_self' => $request->input('self'), 'input_proxy' => $request->input('proxy')]);
                    } else {
                        $voting_log = DB::table('log_votes')->where([['motion', '=', $motion->id], ['user', '=', $user->user]])->first();
                        if($voting_log === null) {
                            if($motion->type === 'public') {
                                DB::beginTransaction();
                                DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->user]);
                                DB::table('votes')->insert(['motion' => $motion->id, 'user' => $user->user, 'vote' => $request->input('self')]);
                                DB::commit();
                            } else {
                                DB::beginTransaction();
                                DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->user]);
                                DB::table('votes')->insert(['motion' => $motion->id, 'vote' => $request->input('self')]);
                                DB::commit();
                            }
                        }

                        if($proxy !== null) {
                            $voting_log_proxy = DB::table('log_votes')->where([['motion', '=', $motion->id], ['user', '=', $user->proxy]])->first();
                            if($voting_log_proxy === null) {
                                if($motion->type === 'public') {
                                    DB::beginTransaction();
                                    DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->proxy]);
                                    DB::table('votes')->insert(['motion' => $motion->id, 'user' => $user->proxy, 'vote' => $request->input('proxy')]);
                                    DB::commit();
                                } else {
                                    DB::beginTransaction();
                                    DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $user->proxy]);
                                    DB::table('votes')->insert(['motion' => $motion->id, 'vote' => $request->input('proxy')]);
                                    DB::commit();
                                }
                            }
                        }

                        return View('motion_completed', ['meeting' => $meeting, 'topic' => $topic, 'motion' => $motion]);
                    }
                } else {
                    $voting_log = DB::table('log_votes')->where([['motion', '=', $motion->id], ['user', '=', $user->user]])->first();
                    if($voting_log !== null)
                        return View('motion_completed', ['meeting' => $meeting, 'topic' => $topic, 'motion' => $motion]);
                    
                    return View('motion', ['meeting' => $meeting, 'topic' => $topic, 'motion' => $motion, 'user' => $user, 'proxy' => $proxy]);
                }
            } else {
                return redirect('/meeting/'.$meeting->id);
            }
        }
    }
    
    public static function helperUserAttending($meeting) {
        if($meeting !== null && parent::user() !== null) {
            $meeting_user = DB::table('meeting_users')->where([['user', '=', parent::user()->id], ['meeting', '=', $meeting->id]])->first();
            if($meeting_user === null) {
                $proxy = null;

                $meeting_proxy = DB::table('meeting_proxies')->where([['meeting', '=', $meeting->id], ['proxy', '=', parent::user()->id]])->first();
                if($meeting_proxy !== null)
                    $proxy = $meeting_proxy->user;

                $meeting_user_proxy = DB::table('meeting_users')->where([['proxy', '=', parent::user()->id], ['meeting', '=', $meeting->id]])->first();
                if($meeting_user_proxy !== null) {
                    DB::table('meeting_users')->where([['user', '=', $meeting_user_proxy->user], ['meeting', '=', $meeting->id]])->update(['proxy' => null]);
                }

                DB::table('meeting_proxies')->where([['user', '=', parent::user()->id], ['meeting', '=', $meeting->id]])->delete();
                DB::table('meeting_users')->insert(['meeting' => $meeting->id, 'user' => parent::user()->id, 'proxy' => $proxy]);
            }
        }
    }
}