<?php
/**
* UserController
*
* Controls user access
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UserController extends Controller {
    public function render(Request $request, $userID = null) {
        if(parent::user() === null)
            return redirect('/login');
        elseif(parent::user()->access !== 'board' && parent::user()->access !== 'administrator')
            return redirect('/');
        
        if($request->is('*/member/*'))
            return self::renderUserMember($request, $userID);
        elseif($request->is('*/board/*'))
            return self::renderUserBoard($request, $userID);
        elseif($request->is('*/delete/*'))
            return self::renderUserDelete($request, $userID);
        else
            return self::renderUser($request);
    }
    
    public function renderUser(Request $request) {
        $users = DB::table('users')->where('status', 'active')->get();
        $administrators = DB::table('users')->where('access', 'administrator')->get();
        
        if($request->method() === 'POST') {
            $error = array();
            
            if($request->input('id') == '')
                $error[] = 'Please fill in a VID';
            
            if($request->input('name') == '')
                $error[] = 'Please fill the users full name';
            
            if(count($error) > 0) {
                return View('admin.user', ['users' => $users, 'error' => $error, 'input_id' => $request->input('id'), 'input_name' => $request->input('name')]);
            } else {
                $user = DB::table('users')->where('id', $request->input('id'))->first();
                if($user !== null) {
                    if($user->access !== 'administrator' && $user->access !== 'board')
                        DB::table('users')->where('id', $request->input('id'))->update(['status' => 'active', 'access' => 'member']);
                    else
                        DB::table('users')->where('id', $request->input('id'))->update(['status' => 'active']);
                } else {
                    DB::table('users')->insert(['id' => $request->input('id'), 'status' => 'active', 'access' => 'member', 'name' => $request->input('name')]);
                }
                
                return redirect('/admin/user');
            }
        }
            
        return View('admin.user', ['users' => $users, 'administrators' => $administrators]);
    }
    
    public function renderUserMember(Request $request, $userID) {
        $user = DB::table('users')->where('id', $userID)->first();
        
        if($user !== null)
            DB::table('users')->where('id', $user->id)->update(['access' => 'member']);
        
        return redirect('/admin/user');
    }
    
    public function renderUserBoard(Request $request, $userID) {
        $user = DB::table('users')->where('id', $userID)->first();
        
        if($user !== null)
            DB::table('users')->where('id', $user->id)->update(['access' => 'board']);
        
        return redirect('/admin/user');
    }
    
    public function renderUserDelete(Request $request, $userID) {
        $user = DB::table('users')->where('id', $userID)->first();
        
        if($user !== null)
            DB::table('users')->where('id', $user->id)->delete();
        
        return redirect('/admin/user');
    }
}