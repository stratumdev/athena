<?php
/**
* DashboardController
*
* Dashboard overview for administrators
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller {
    public function render(Request $request) {
        if(parent::user() === null)
            return redirect('/login');
        elseif(parent::user()->access !== 'board' && parent::user()->access !== 'administrator')
            return redirect('/');
        
        $meetings = DB::table('meetings')->orderBy('start', 'desc')->get();
        if(count($meetings) > 0) {
            foreach($meetings AS $meeting)
                $meeting->chairman = json_decode($meeting->chairman);
        }

        return View('admin.dashboard', ['meetings' => $meetings]);
    }
}