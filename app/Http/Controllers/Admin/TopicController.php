<?php
/**
* MeetingController
*
* Administrator tools for meetings
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class TopicController extends Controller {
    public function render(Request $request, $meetingID = null, $topicID = null) {
        if(parent::user() === null)
            return redirect('/login');
        elseif(parent::user()->access !== 'board' && parent::user()->access !== 'administrator')
            return redirect('/');
        
        if($request->is('*/new'))
            return self::renderTopicNew($request, $meetingID);
        
        if($request->is('*/sort'))
            return self::ajaxTopicSort($request, $meetingID);
                        
        if($request->is('*/delete/*'))
            return self::renderTopicDelete($request, $meetingID, $topicID);

        if($request->is('*/open/*'))
            return self::renderTopicOpen($request, $meetingID, $topicID);

        if($request->is('*/close/*'))
            return self::renderTopicClose($request, $meetingID, $topicID);

        if($request->is('*/edit/*'))
            return self::renderTopicEdit($request, $meetingID, $topicID);
    }
    
    public function renderTopicNew(Request $request, $meetingID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        
        if($meeting !== null && ($meeting->status == 'preparation' || $meeting->status == 'open')) {
            if($request->method() === 'POST') {
                $error = array();
                $author = DB::table('users')->where('id', $request->input('author'))->first();

                if($request->input('author') == '')
                    $error[] = 'Please fill the VID of the author';
                elseif(!preg_match('/[0-9]{6}/', $request->input('author')))
                    $error[] = 'Author VID invalid';
                elseif($author === null)
                    $error[] = 'Author VID not found in user database';

                if($request->input('title') == '')
                    $error[] = 'Please fill in a title';

                if($request->input('description') == '')
                    $error[] = 'Please fill in a description';

                if(count($error) > 0) {
                    return View('admin.topic_new', ['meeting' => $meeting, 'error' => $error, 'input_author' => $request->input('author'), 'input_title' => $request->input('title'), 'input_description' => $request->input('description')]);
                } else {
                    DB::table('topics')->insert(['meeting' => $meeting->id, 'author' => json_encode(['id' => $author->id, 'name' => $author->name]), 'title' => $request->input('title'), 'description' => $request->input('description')]);

                    return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.DB::getPdo()->lastInsertId());
                }
            }
            
            return View('admin.topic_new', ['meeting' => $meeting]);
        }
    }
    
    public function renderTopicEdit(Request $request, $meetingID, $topicID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        
        if($meeting !== null && $topic !== null) {
            $topic->author = json_decode($topic->author);
            
            $motions = DB::table('motions')->where('topic', $topic->id)->get();
            if(count($motions) > 0) {
                foreach($motions AS $motion)
                    $motion->author = json_decode($motion->author);
            }
            
            if($request->method() === 'POST') {
                $error = array();
                $author = DB::table('users')->where('id', $request->input('author'))->first();
                
                if($request->input('author') == '')
                    $error[] = 'Please fill the VID of the author';
                elseif(!preg_match('/[0-9]{6}/', $request->input('author')))
                    $error[] = 'Author VID invalid';
                elseif($author === null)
                    $error[] = 'Author VID not found in user database';

                if($request->input('title') == '')
                    $error[] = 'Please fill in a title';

                if($request->input('description') == '')
                    $error[] = 'Please fill in a description';
                
                if(count($error) > 0) {
                    return View('admin.topic', ['meeting' => $meeting, 'topic' => $topic, 'motions' => $motions, 'error' => $error, 'input_author' => $request->input('author'), 'input_title' => $request->input('title'), 'input_description' => $request->input('description')]);
                } else {
                    DB::table('topics')->where('id', $topic->id)->update(['meeting' => $meeting->id, 'author' => json_encode(['id' => $author->id, 'name' => $author->name]), 'title' => $request->input('title'), 'description' => $request->input('description')]);

                    return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id);
                }
            }
            
            return View('admin.topic', ['meeting' => $meeting, 'topic' => $topic, 'motions' => $motions, 'input_author' => $topic->author->id, 'input_title' => $topic->title, 'input_description' => $topic->description]);
        }
    }
    
    public function renderTopicDelete(Request $request, $meetingID, $topicID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        
        if($meeting !== null && $topic !== null) {
            $motions = DB::table('motions')->where('topic', $topic->id)->get();
            if(count($motions) == 0)
                DB::table('topics')->where('id', $topicID)->delete();
            
            return redirect('/admin/meeting/edit/'.$meeting->id);
        }
    }
    
    public function renderTopicOpen(Request $request, $meetingID, $topicID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        
        if($meeting !== null && $topic !== null) {
            DB::table('motions')->where([['topic', '=', $topic->id], ['status', '!=', 'closed']])->update(['status' => 'open']);
            
            return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id);
        }
    }
    
    public function renderTopicClose(Request $request, $meetingID, $topicID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        
        if($meeting !== null && $topic !== null) {
            $motions = DB::table('motions')->where([['topic', '=', $topic->id], ['status', '=', 'open']])->get();
            if(count($motions) > 0) {
                foreach($motions AS $motion) {
                    MotionController::helperMotionClose($meetingID, $motion);
                }
            }
            
            return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id);
        }
    }
    
    public function ajaxTopicSort(Request $request, $meetingID) {
        if($request->input('order') !== null && count($request->input('order'))) {
            $i = 0;
            foreach($request->input('order') AS $ID) {
                DB::table('topics')->where('id', $ID)->update(['sort' => $i]);
                $i++;
            }
        }
    }
}