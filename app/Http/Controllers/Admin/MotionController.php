<?php
/**
* MotionController
*
* Administrator motions for meetings
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MotionController extends Controller {
    public function render(Request $request, $meetingID = null, $topicID = null, $motionID = null) {
        if(parent::user() === null)
            return redirect('/login');
        elseif(parent::user()->access !== 'board' && parent::user()->access !== 'administrator')
            return redirect('/');
        
        if($request->is('*/new'))
            return self::renderMotionNew($request, $meetingID, $topicID);
        
        if($request->is('*/open/*'))
            return self::renderMotionOpen($request, $meetingID, $topicID, $motionID);
        
        if($request->is('*/close/*'))
            return self::renderMotionClose($request, $meetingID, $topicID, $motionID);
        
        if($request->is('*/delete/*'))
            return self::renderMotionDelete($request, $meetingID, $topicID, $motionID);
        
        if($request->is('*/edit/*'))
            return self::renderMotionEdit($request, $meetingID, $topicID, $motionID);
    }
    
    public function renderMotionNew(Request $request, $meetingID, $topicID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();

        if($meeting !== null && ($meeting->status === 'preparation' || $meeting->status === 'open') && $topic !== null) {
            if($request->method() === 'POST') {
                $error = array();
                $author = DB::table('users')->where('id', $request->input('author'))->first();

                if($request->input('author') == '')
                    $error[] = 'Please fill the VID of the author';
                elseif(!preg_match('/[0-9]{6}/', $request->input('author')))
                    $error[] = 'Author VID invalid';
                elseif($author === null)
                    $error[] = 'Author VID not found in user database';

                if($request->input('description') == '')
                    $error[] = 'Please fill in a description';

                if(count($error) > 0) {
                    return View('admin.motion_new', ['meeting' => $meeting, 'topic' => $topic, 'error' => $error, 'input_author' => $request->input('author'), 'input_description' => $request->input('description'), 'input_type' => $request->input('type'), 'input_majority' => $request->input('majority')]);
                } else {
                    DB::table('motions')->insert(['topic' => $topic->id, 'author' => json_encode(['id' => $author->id, 'name' => $author->name]), 'description' => $request->input('description'), 'type' => $request->input('type'), 'majority' => $request->input('majority')]);

                    return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id);
                }
            }
            
            return View('admin.motion_new', ['meeting' => $meeting, 'topic' => $topic]);
        }
    }
    
    public function renderMotionEdit(Request $request, $meetingID, $topicID, $motionID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        $motion = self::helperMotionGet($motionID);
        
        if($meeting !== null && $topic !== null && $motion !== null) {
            if($request->method() === 'POST') {
                $error = array();
                $author = DB::table('users')->where('id', $request->input('author'))->first();

                if($request->input('author') == '')
                    $error[] = 'Please fill the VID of the author';
                elseif(!preg_match('/[0-9]{6}/', $request->input('author')))
                    $error[] = 'Author VID invalid';
                elseif($author === null)
                    $error[] = 'Author VID not found in user database';
                
                if($request->input('description') == '')
                    $error[] = 'Please fill in a description';

                if(count($error) > 0) {
                    return View('admin.motion', ['meeting' => $meeting, 'topic' => $topic, 'motion' => $motion, 'error' => $error, 'input_author' => $request->input('author'), 'input_description' => $request->input('description'), 'input_type' => $request->input('type'), 'input_majority' => $request->input('majority')]);
                } else {
                    DB::table('motions')->where('id', $motion->id)->update(['author' => json_encode(['id' => $author->id, 'name' => $author->name]), 'description' => $request->input('description'), 'type' => $request->input('type'), 'majority' => $request->input('majority')]);
                    
                    return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id.'/motion/edit/'.$motion->id);
                }
            }
                        
            $result = null;
            
            if($motion->result !== null)
                $result = json_decode($motion->result, true);
            
            if($result !== null && array_key_exists('votes', $result) && count($result['votes']) > 0) {
                $agree_votes = array();
                $disagree_votes = array();
                $abstain_votes = array();
                
                foreach($result['votes'] AS $vote) {
                    if($vote['vote'] === 'agree')
                        $agree_votes[] = $vote['name'].' ('.$vote['user'].')';
                    elseif($vote['vote'] === 'disagree')
                        $disagree_votes[] = $vote['name'].' ('.$vote['user'].')';
                    elseif($vote['vote'] === 'abstain')
                        $abstain_votes[] = $vote['name'].' ('.$vote['user'].')';
                }
                
                $result['agree_votes'] = $agree_votes;
                $result['disagree_votes'] = $disagree_votes;
                $result['abstain_votes'] = $abstain_votes;
            }
            
            $waitingfor = DB::table('meeting_users')->where([['meeting', '=', $meeting->id]])->whereNotIn('user', DB::table('log_votes')->where('motion', $motion->id)->pluck('user'))->get();
            if(count($waitingfor) > 0) {
                foreach($waitingfor AS $waitingf) {
                    $user = DB::table('users')->where('id', $waitingf->user)->first();
                    
                    if($user !== null)
                        $waitingf->name = $user->name;
                }
            }
            
            return View('admin.motion', ['meeting' => $meeting, 'topic' => $topic, 'motion' => $motion, 'input_author' => $motion->author->id, 'input_description' => $motion->description, 'input_type' => $motion->type, 'input_majority' => $motion->majority, 'waitingfor' => $waitingfor, 'result' => $result]);
        }
    }
    
    public function renderMotionDelete(Request $request, $meetingID, $topicID, $motionID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        $motion = self::helperMotionGet($motionID);
        
        if($meeting !== null && $topic !== null && $motion !== null && $motion->status === 'preparation') {
            DB::table('motions')->where('id', $motion->id)->delete();
            
            return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id);
        }
    }
    
    public function renderMotionOpen(Request $request, $meetingID, $topicID, $motionID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        $motion = self::helperMotionGet($motionID);
        
        if($meeting !== null && $topic !== null && $motion !== null && $motion->status === 'preparation') {
            DB::table('motions')->where('id', $motion->id)->update(['status' => 'open']);
            
            return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id);
        }
    }
    
    public function renderMotionClose(Request $request, $meetingID, $topicID, $motionID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        $topic = DB::table('topics')->where('id', $topicID)->first();
        $motion = self::helperMotionGet($motionID);
        
        if($meeting !== null && $topic !== null && $motion !== null) {
            self::helperMotionClose($meetingID, $motion);
            
            return redirect('/admin/meeting/edit/'.$meeting->id.'/topic/edit/'.$topic->id.'/motion/edit/'.$motion->id);
        }
    }
    
    public static function helperMotionClose($meetingID, $motion) {
        if($motion !== null && $motion->status === 'open') {            
            $waitingfor = DB::table('meeting_users')->where([['meeting', '=', $meetingID]])->whereNotIn('user', DB::table('log_votes')->where('motion', $motion->id)->pluck('user'))->get();
            foreach($waitingfor AS $waitingf) {
                if($motion->type === 'public') {
                    DB::beginTransaction();
                    DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $waitingf->user]);
                    DB::table('votes')->insert(['motion' => $motion->id, 'user' => $waitingf->user, 'vote' => 'abstain']);
                    DB::commit();
                } else {
                    DB::beginTransaction();
                    DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $waitingf->user]);
                    DB::table('votes')->insert(['motion' => $motion->id, 'vote' => 'abstain']);
                    DB::commit();
                }
                
                if($waitingf->proxy !== null) {
                    if($motion->type === 'public') {
                        DB::beginTransaction();
                        DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $waitingf->proxy]);
                        DB::table('votes')->insert(['motion' => $motion->id, 'user' => $waitingf->proxy, 'vote' => 'abstain']);
                        DB::commit();
                    } else {
                        DB::beginTransaction();
                        DB::table('log_votes')->insert(['motion' => $motion->id, 'user' => $waitingf->proxy]);
                        DB::table('votes')->insert(['motion' => $motion->id, 'vote' => 'abstain']);
                        DB::commit();
                    }
                }
            }
            
            $votes = DB::table('votes')->where('motion', $motion->id)->get();

            $votesAgree = 0;
            $votesDisagree = 0;
            $votesAbstain = 0;

            $voteList = array();
            $result = null;

            $outcome = false;

            foreach($votes AS $vote) {
                if($vote->vote === 'agree')
                    $votesAgree++;
                elseif($vote->vote === 'disagree')
                    $votesDisagree++;
                elseif($vote->vote === 'abstain')
                    $votesAbstain++;

                if($motion->type === 'public') {
                    $user = DB::table('users')->where('id', $vote->user)->first();

                    $name = null;
                    if($user !== null)
                        $name = $user->name;

                    $voteList[] = array('user' => $vote->user, 'name' => $name, 'vote' => $vote->vote);
                }
            }

            if($motion->majority === 'regular') {
                if($votesAgree > $votesDisagree)
                    $outcome = 'agree';
                elseif($votesAgree == $votesDisagree)
                    $outcome = 'tie';
                else
                    $outcome = 'disagree';
            } elseif($motion->majority === 'twothird') {
                if($votesAgree >= (floor(($votesAgree + $votesDisagree) / 3 * 2) + 1))
                    $outcome = 'agree';
                else
                    $outcome = 'disagree';
            }

            $result = array('agree' => $votesAgree, 'disagree' => $votesDisagree, 'abstain' => $votesAbstain, 'outcome' => $outcome);
            
            if($motion->type === 'public')
                $result['votes'] = $voteList;

            DB::table('motions')->where('id', $motion->id)->update(['result' => json_encode($result), 'status' => 'closed']);
        }
    }
    
    public static function helperMotionGet($motionID) {
        $motion = DB::table('motions')->where('id', $motionID)->first();
        
        if($motion !== null) {
            $motion->author = json_decode($motion->author);
            
            return $motion;
        }
    }
}