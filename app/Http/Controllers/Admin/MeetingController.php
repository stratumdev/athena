<?php
/**
* MeetingController
*
* Administrator tools for meetings
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers\Admin;

use Mail;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MeetingController extends Controller {
    public function render(Request $request, $meetingID = null) {
        if(parent::user() === null)
            return redirect('/login');
        elseif(parent::user()->access !== 'board' && parent::user()->access !== 'administrator')
            return redirect('/');
        
        if($request->is('*/new'))
            return self::renderMeetingNew($request);

        if($request->is('*/open/*'))
            return self::renderMeetingOpen($request, $meetingID);
                
        if($request->is('*/close/*'))
            return self::renderMeetingClose($request, $meetingID);
                
        if($request->is('*/publish/*'))
            return self::renderMeetingPublish($request, $meetingID);
                
        if($request->is('*/delete/*'))
            return self::renderMeetingDelete($request, $meetingID);

        if($request->is('*/edit/*'))
            return self::renderMeetingEdit($request, $meetingID);
    }
    
    public function renderMeetingNew(Request $request) {
        if($request->method() === 'POST') {
            $error = array();
            $chairman = DB::table('users')->where('id', $request->input('chairman'))->first();
            
            if($request->input('date') == '')
                $error[] = 'Please fill in a date';
            
            if($request->input('start') == '')
                $error[] = 'Please fill in a starting time in UTC';
            
            if($request->input('end') == '')
                $error[] = 'Please fill in a ending time in UTC';
            
            if($request->input('chairman') == '')
                $error[] = 'Please fill in a meeting chairman';
            elseif(!preg_match('/[0-9]{6}/', $request->input('chairman')))
                $error[] = 'Chairman VID invalid';
            elseif($chairman === null)
                $error[] = 'Chairman VID not found in user database';

            if($request->input('calledby') == '')
                $error[] = 'Please fill in who called the meeting';
            
            if(count($error) > 0) {
                return View('admin.meeting_new', ['error' => $error, 'input_type' => $request->input('type'), 'input_date' => $request->input('date'), 'input_start' => $request->input('start'), 'input_end' => $request->input('end'), 'input_chairman' => $request->input('chairman'), 'input_calledby' => $request->input('calledby')]);
            } else {
                $start = new Datetime($request->input('date').' '.$request->input('start'));
                $end = new Datetime($request->input('date').' '.$request->input('end'));
                
                DB::table('meetings')->insert(['start' => $start, 'end' => $end, 'chairman' => json_encode(['id' => $chairman->id, 'name' => $chairman->name]), 'calledby' => $request->input('calledby')]);
                
                return redirect('/admin/meeting/edit/'.DB::getPdo()->lastInsertId());
            }
        }
            
        return View('admin.meeting_new');
    }
    
    public function renderMeetingEdit(Request $request, $meetingID) {
        $meeting = self::helperMeetingGet($meetingID);
        
        if($meeting !== null && $meeting->status !== 'published') {
            $topics = DB::table('topics')->where('meeting', $meeting->id)->orderBy('sort', 'ASC')->get();
            if(count($topics) > 0) {
                foreach($topics AS $topic)
                    $topic->author = json_decode($topic->author);
            }
            
            $start = new Datetime($meeting->start);
            $end = new Datetime($meeting->end);
            
            if($request->method() === 'POST') {
                $error = array();
                $chairman = DB::table('users')->where('id', $request->input('chairman'))->first();

                if($request->input('date') == '')
                    $error[] = 'Please fill in a date';

                if($request->input('start') == '')
                    $error[] = 'Please fill in a starting time in UTC';

                if($request->input('end') == '')
                    $error[] = 'Please fill in a ending time in UTC';

                if($request->input('chairman') == '')
                    $error[] = 'Please fill in a meeting chairman';
                elseif(!preg_match('/[0-9]{6}/', $request->input('chairman')))
                    $error[] = 'Chairman VID invalid';
                elseif($chairman === null)
                    $error[] = 'Chairman VID not found in user database';

                if($request->input('calledby') == '')
                    $error[] = 'Please fill in who called the meeting';

                if(count($error) > 0) {
                    return View('admin.meeting', ['meeting' => $meeting, 'topics' => $topics, 'error' => $error, 'input_type' => $request->input('type'), 'input_date' => $request->input('date'), 'input_start' => $request->input('start'), 'input_end' => $request->input('end'), 'input_chairman' => $request->input('chairman'), 'input_calledby' => $request->input('calledby'), 'input_notes' => $request->input('notes')]);
                } else {
                    $start = new Datetime($request->input('date').' '.$request->input('start'));
                    $end = new Datetime($request->input('date').' '.$request->input('end'));
                    
                    DB::table('meetings')->where('id', $meeting->id)->update(['start' => $start, 'end' => $end, 'chairman' => json_encode(['id' => $chairman->id, 'name' => $chairman->name]), 'calledby' => $request->input('calledby')]);
                    
                    if($meeting->status === 'preparation' && $meeting->type !== $request->input('type'))
                       DB::table('meetings')->where('id', $meeting->id)->update(['type' => $request->input('type')]);
                    
                    if($meeting->status === 'closed' && $request->input('notes') !== '')
                        DB::table('meetings')->where('id', $meeting->id)->update(['notes' => $request->input('notes')]);
                    
                    return redirect('/admin/meeting/edit/'.$meeting->id);
                }
            }
            
            return View('admin.meeting', ['meeting' => $meeting, 'topics' => $topics, 'input_type' => $meeting->type, 'input_date' => $start->format('Y-m-d'), 'input_start' => $start->format('H:i'), 'input_end' => $end->format('H:i'), 'input_chairman' => $meeting->chairman->id, 'input_calledby' => $meeting->calledby, 'input_notes' => $meeting->notes]);
        }
    }
    
    public function renderMeetingDelete(Request $request, $meetingID) {
        $meeting = self::helperMeetingGet($meetingID);
        
        if($meeting !== null && $meeting->status === 'preparation') {
            $topics = DB::table('topics')->where('meeting', $meeting->id)->get();
            
            if(count($topics) > 0) {
                foreach($topics AS $topic) {
                    DB::table('motions')->where('topic', $topic->id)->delete();
                }
                
                DB::table('topics')->where('meeting', $meeting->id)->delete();
            }
            
            DB::table('meetings')->where('id', $meeting->id)->delete();
            
            return redirect('/admin');
        }
    }
    
    public function renderMeetingOpen(Request $request, $meetingID) {
        $meeting = self::helperMeetingGet($meetingID);
        
        if($meeting !== null && $meeting->status === 'preparation')
            DB::table('meetings')->where('id', $meeting->id)->update(['start' => new Datetime(), 'status' => 'open']);
        
        return redirect('/admin/meeting/edit/'.$meeting->id);
    }
    
    public function renderMeetingClose(Request $request, $meetingID) {
        $meeting = self::helperMeetingGet($meetingID);
        
        if($meeting !== null) {
            $users = array();
            $users_present = array();
            $users_proxy = array();
            $users_invited = DB::table('users')->where('status', 'active')->get();

            /* Attendance array */
            if(count($users_invited) > 0) {
                foreach($users_invited AS $user) {
                    $users[$user->id] = 'absent';
                }

                $meeting_proxies = DB::table('meeting_proxies')->where('meeting', $meeting->id)->get();
                if(count($meeting_proxies) > 0) {
                    foreach($meeting_proxies AS $meeting_proxy) {
                        $proxy = DB::table('users')->where('id', $meeting_proxy->proxy)->first();
                        if($proxy !== null) {
                            $users[$meeting_proxy->user] = 'proxy-absent';
                            $users_proxy[$meeting_proxy->user] = $proxy->id.' - '.$proxy->name;
                        }
                    }
                }
                
                $meeting_users = DB::table('meeting_users')->where('meeting', $meeting->id)->get();
                if(count($meeting_users) > 0) {
                    foreach($meeting_users AS $meeting_user) {
                        $users[$meeting_user->user] = 'present';

                        if($meeting_user->proxy !== null) {
                            $users[$meeting_user->proxy] = 'proxied';
                        }
                    }
                }
            }
            
            if(count($users) > 0) {
                foreach($users AS $id => $status) {
                    $user = DB::table('users')->where('id', $id)->first();
                    
                    if($user !== null)
                        $users_present[$id] = ['name' => $user->name, 'status' => $status];
                    
                    if($status === 'proxied' || $status === 'proxy-absent')
                        $users_present[$id]['proxy'] = $users_proxy[$id];
                }
            }
            
            if(count($users_present) > 0)
                DB::table('meetings')->where('id', $meeting->id)->update(['present' => json_encode($users_present)]);

            /* Topics (with motions) array */
            $topics = DB::table('topics')->where('meeting', $meeting->id)->orderBy('sort', 'ASC')->get();
            if(count($topics) > 0) {                
                foreach($topics AS $topic) {
                    $topic->author = json_decode($topic->author);
                    $motions = DB::table('motions')->where('topic', $topic->id)->get();
                    
                    if(count($motions) > 0) {
                        foreach($motions AS $motion) {
                            $motion->author = json_decode($motion->author);
                            if($motion->result !== null)
                                $motion->result = json_decode($motion->result, true);
                        }
                        
                        $topic->motions = $motions;
                    }
                }
            } else {
                $topics = null;
            }
                        
            if($meeting !== null && $meeting->status === 'open')
                DB::table('meetings')->where('id', $meeting->id)->update(['topics' => $topics, 'end' => new Datetime(), 'status' => 'closed']);

            return redirect('/admin/meeting/edit/'.$meeting->id);
        }
    }
    
    public function renderMeetingPublish(Request $request, $meetingID) {
        $meeting = self::helperMeetingGet($meetingID);
        
        if($meeting !== null && $meeting->status === 'closed') {
            DB::table('meetings')->where('id', $meeting->id)->update(['status' => 'published']);
        
            $addresses = DB::table('users')->whereNotNull('email')->get();
            if($addresses !== null && count($addresses) > 0) {
                /* Mail members about the publishment */
                Mail::send(array('text' => 'email'), ['pdf_url' => env('APP_URL').'/report/pdf/'.$meeting->id], function ($m) use ($addresses) {
                    $m->from('npo@ivao.aero', 'IVAO NPO');

                    foreach($addresses AS $address) {
                        $m->bcc($address->email, $address->name);
                    }
                    
                    $m->subject('Meeting Published');
                });
            }
        
            /* Save the current database data in a dumpfile as backup */
            $dump = [];
            $tables = array_map('reset', \DB::select('SHOW TABLES'));
            if($tables !== null && count($tables) > 0) {
                foreach($tables AS $table) {
                    $output = DB::select("SELECT * FROM ".$table);
                    $dump[$table] = (count($output) > 0) ? $output : null;
                }
            }

            Storage::put('./dbbackups/'.date('YmdHis').'.dbb', json_encode($dump), 'private');
        }
        
        return redirect('/admin');
    }
    
    public static function helperMeetingGet($meetingID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        
        if($meeting !== null) {
            $meeting->chairman = json_decode($meeting->chairman);
            
            return $meeting;
        }
    }
}