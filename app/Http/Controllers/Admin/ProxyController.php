<?php
/**
* Proxycontroller
*
* Administrator for managing proxies
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProxyController extends Controller {
    public function render(Request $request, $meetingID = null, $userID = null) {
        if(parent::user() === null)
            return redirect('/login');
        elseif(parent::user()->access !== 'board' && parent::user()->access !== 'administrator')
            return redirect('/');
        
        if($request->is('*/delete/*'))
            return self::renderProxyDelete($request, $meetingID, $userID);
        else
            return self::renderProxy($request, $meetingID);
    }
    
    public function renderProxy(Request $request, $meetingID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        
        if($meeting !== NULL && $meeting->type === 'GA' && ($meeting->status === 'preparation' || $meeting->status === 'open')) {
            $proxies = DB::table('meeting_proxies')->where('meeting', $meeting->id)->get();
            
            if($request->method() === 'POST') {
                $error = array();
                
                if($request->input('user') == '')
                    $error[] = 'Please fill in a VID From';
                elseif(DB::table('users')->where('id', $request->input('user'))->count() != 1)
                    $error[] = 'VID From does not exists';
                elseif(DB::table('meeting_proxies')->where([['meeting', '=', $meeting->id], ['proxy', '=', $request->input('user')]])->count() > 0)
                    $error[] = 'This user already is a proxy for another user';
                elseif(DB::table('meeting_proxies')->where([['meeting', '=', $meeting->id], ['user', '=', $request->input('user')]])->count() > 0)
                    $error[] = 'This user already has a proxy';
                
                if($request->input('proxy') == '')
                    $error[] = 'Please fill in a VID To';
                elseif(DB::table('users')->where('id', $request->input('proxy'))->count() != 1)
                    $error[] = 'VID To does not exists';
                elseif(DB::table('meeting_proxies')->where([['meeting', '=', $meeting->id], ['proxy', '=', $request->input('proxy')]])->count() > 0)
                    $error[] = 'This user already is a proxy for another user';
                elseif(DB::table('meeting_proxies')->where([['meeting', '=', $meeting->id], ['user', '=', $request->input('proxy')]])->count() > 0)
                    $error[] = 'This user already has a proxy';

                if(count($error) > 0) {
                    return View('admin.proxy', ['meeting' => $meeting, 'proxies' => $proxies, 'error' => $error, 'input_user' => $request->input('user'), 'input_proxy' => $request->input('proxy')]);
                } else {
                    DB::table('meeting_proxies')->insert(['meeting' => $meeting->id, 'user' => $request->input('user'), 'proxy' => $request->input('proxy')]);
                    
                    if($meeting->status === 'open') {
                        $meeting_user = DB::table('meeting_users')->where('user', $request->input('proxy'))->first();
                        if($meeting_user !== null)
                            DB::table('meeting_users')->where('user', $request->input('proxy'))->update(['proxy' => $request->input('user')]);
                        
                        $meeting_user = DB::table('meeting_users')->where('user', $request->input('user'))->first();
                        if($meeting_user !== null)
                            DB::table('meeting_users')->where('user', $request->input('user'))->delete();
                    }

                    return redirect('/admin/meeting/edit/'.$meeting->id.'/proxy');
                }
            }
            
            return View('admin.proxy', ['meeting' => $meeting, 'proxies' => $proxies]);
        }
    }
    
    public function renderProxyDelete(Request $request, $meetingID, $userID) {
        $meeting = DB::table('meetings')->where('id', $meetingID)->first();
        
        if($meeting !== null && $meeting->type === 'GA' && ($meeting->status === 'preparation' || $meeting->status === 'open')) {
            if($meeting->status === 'open') {
                $meeting_user = DB::table('meeting_users')->where('proxy', $userID)->first();
                if($meeting_user !== null)
                    DB::table('meeting_users')->where('proxy', $userID)->update(['proxy' => null]);
            }
            
            DB::table('meeting_proxies')->where([['meeting', '=', $meeting->id], ['user', '=', $userID]])->delete();
            return redirect('/admin/meeting/edit/'.$meeting->id.'/proxy');
        }
    }
}