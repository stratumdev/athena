<?php
/**
* HelperController
*
* Static functions for retrieving and storing database information
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class HelperController extends Controller {
    public static function getMeeting($filter = null) {
        
    }
    
    public static function getMeetings($filter = null) {
        
    }
    
    public static function getTopic($filter = null) {
        
    }
    
    public static function getTopics($filter = null) {
        
    }
    
    public static function getMotion($filter = null) {
        
    }
    
    public static function getMotions($filter = null) {
        
    }
    
    public static function closeMotion($motion) {
        
    }
}