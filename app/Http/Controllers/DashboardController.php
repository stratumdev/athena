<?php
/**
* DashboardController
*
* Checking whether user is logged in and either showing them all metings or redirecting them to the login
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller {
    public function render(Request $request) {
        if(parent::user() !== null) {
            $active_meetings = null;
            if(parent::user()->status === 'active' && (parent::user()->access === 'member' || parent::user()->access === 'board' || parent::user()->access === 'administrator')) {
                if(parent::user()->access === 'board' || parent::user()->access === 'administrator')
                    $active_meetings = DB::table('meetings')->where('status', 'open')->orderBy('start', 'asc')->get();
                else
                    $active_meetings = DB::table('meetings')->where([['type', '=', 'ga'], ['status', '=', 'open']])->orderBy('start', 'asc')->get();
            }
            
            $meetings = DB::table('meetings')->where([['status', '!=', 'preparation']])->orderBy('start', 'desc')->get();
            if(count($meetings) > 0) {
                foreach($meetings AS $meeting)
                    $meeting->chairman = json_decode($meeting->chairman);
            }
            
            return View('meetings', array('meetings' => $meetings, 'active_meetings' => $active_meetings));
        } else {
            return redirect('/login');
        }
    }
    
    public function account_edit(Request $request) {
        if(parent::user() === null)
            return redirect('/login');
        
        if($request->method() === 'POST') {
            if($request->input('email') === null || !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) === false) {
                DB::table('users')->where('id', parent::user()->id)->update(['email' => $request->input('email')]);
            } else {
                $error = 'Please fill in a valid e-mailaddress (or make the field empty to remove it)';
            }
            
            $input['email'] = $request->input('email');
        } else {
            $input['email'] = parent::user()->email;
        }
        
        return View('account_edit', ['error' => (isset($error) ? $error : null), 'input' => (isset($input) ? $input : null)]);
    }
}