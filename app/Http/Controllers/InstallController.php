<?php
/**
* InstallController
*
* Controller responsible for installing and upgrading the system
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
* @license MIT
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Http\Controllers\Controller;

class InstallController extends Controller {
    public function render(Request $request) {
        if($request->path() === 'install')
            return self::install();
    }
    
    public function install() {
        if(env('APP_DEBUG') === TRUE) {
            DB::statement("DROP TABLE IF EXISTS `log_votes`");
            DB::statement("DROP TABLE IF EXISTS `meeting_proxies`");
            DB::statement("DROP TABLE IF EXISTS `meeting_users`");
            DB::statement("DROP TABLE IF EXISTS `meetings`");
            DB::statement("DROP TABLE IF EXISTS `motions`");
            DB::statement("DROP TABLE IF EXISTS `sessions`");
            DB::statement("DROP TABLE IF EXISTS `topics`");
            DB::statement("DROP TABLE IF EXISTS `users`");
            DB::statement("DROP TABLE IF EXISTS `votes`");
        }
        
        if(!Schema::hasTable('log_votes'))
            DB::statement("CREATE TABLE `log_votes` (`motion` int(11) NOT NULL, `user` int(11) NOT NULL, PRIMARY KEY (`motion`,`user`), KEY `motion` (`motion`), KEY `user` (`user`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('meeting_proxies'))
            DB::statement("CREATE TABLE `meeting_proxies` (`meeting` int(11) NOT NULL, `user` int(11) NOT NULL, `proxy` int(11) NOT NULL, PRIMARY KEY (`meeting`,`user`,`proxy`), KEY `meeting` (`meeting`), KEY `user` (`user`), KEY `proxy` (`proxy`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('meeting_users'))
            DB::statement("CREATE TABLE `meeting_users` (`meeting` int(11) NOT NULL, `user` int(11) NOT NULL, `proxy` int(11) DEFAULT NULL, PRIMARY KEY (`meeting`,`user`), KEY `meeting` (`meeting`), KEY `user` (`user`), KEY `proxy` (`proxy`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('meetings'))
            DB::statement("CREATE TABLE `meetings` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `version` int(1) unsigned NOT NULL DEFAULT '1', `status` enum('preparation','open','closed','published') NOT NULL DEFAULT 'preparation', `type` enum('GA','BoG') NOT NULL DEFAULT 'GA', `start` datetime NOT NULL, `end` datetime NOT NULL, `chairman` varchar(256) NOT NULL, `calledby` varchar(128) NOT NULL DEFAULT '', `present` longtext, `topics` longtext, `notes` longtext, PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('motions'))
            DB::statement("CREATE TABLE `motions` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `topic` int(11) NOT NULL, `author` varchar(256) NOT NULL, `status` enum('preparation','open','closed') NOT NULL DEFAULT 'preparation', `description` text NOT NULL, `type` enum('public','anonymous') NOT NULL DEFAULT 'public', `majority` enum('regular','twothird','fourfifth') NOT NULL DEFAULT 'regular', `result` longtext, PRIMARY KEY (`id`), KEY `topic` (`topic`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('sessions'))
            DB::statement("CREATE TABLE `sessions` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `user` int(11) NOT NULL, `datetime` datetime NOT NULL, `iphash` varchar(128) NOT NULL, PRIMARY KEY (`id`), KEY `user` (`user`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('topics'))
            DB::statement("CREATE TABLE `topics` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `meeting` int(11) NOT NULL, `author` varchar(256) NOT NULL, `title` varchar(512) NOT NULL DEFAULT '', `description` text NOT NULL, `sort` int(3) DEFAULT NULL, PRIMARY KEY (`id`), KEY `meeting` (`meeting`)) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('users'))
            DB::statement("CREATE TABLE `users` (`id` int(11) unsigned NOT NULL, `status` enum('active','inactive') NOT NULL DEFAULT 'inactive', `access` enum('adjoined','member','board','administrator') DEFAULT 'member', `name` varchar(256) NOT NULL DEFAULT '', `email` varchar(512) DEFAULT NULL, PRIMARY KEY (`id`), KEY `status` (`status`), KEY `access` (`access`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        if(!Schema::hasTable('votes'))
            DB::statement("CREATE TABLE `votes` (`motion` int(11) unsigned NOT NULL, `user` int(11) unsigned DEFAULT NULL, `vote` enum('agree','disagree','abstain') NOT NULL DEFAULT 'abstain', KEY `motion` (`motion`), KEY `user` (`user`), KEY `vote` (`vote`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        if(env('ADMINISTRATOR') !== '')
            DB::statement("INSERT INTO `users` (`id`, `status`, `access`, `name`) VALUES (".env('ADMINISTRATOR').", 'active', 'administrator', 'Administrator');");
        
        if(env('APP_DEBUG') === TRUE) {
            DB::statement("INSERT INTO `meetings` (`id`, `status`, `type`, `start`, `end`, `chairman`, `calledby`, `present`, `topics`, `notes`) VALUES (NULL, 'preparation', 'GA', '2017-02-11 18:00:00', '2017-02-11 21:00:00', '{\"id\":232726,\"name\":\"Pim Oude Veldhuis\"}', 'BoG', NULL, NULL, NULL);");
            DB::statement("INSERT INTO `topics` (`id`, `meeting`, `author`, `title`, `description`) VALUES (1, 1, '{\"id\":232726,\"name\":\"Pim Oude Veldhuis\"}', 'This is a sample topic', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut arcu augue. Praesent nec laoreet nisi, venenatis convallis libero. Donec pharetra nec est ac tempus. Aenean at tortor finibus, facilisis leo in, ultrices libero. Vivamus vulputate commodo urna laoreet luctus. Vestibulum pellentesque orci id congue auctor. Morbi a sollicitudin est.');");
            DB::statement("INSERT INTO `motions` (`id`, `topic`, `author`, `status`, `description`, `type`, `majority`, `result`) VALUES (1, 1, '{\"id\":232726,\"name\":\"Pim Oude Veldhuis\"}', 'preparation', 'This is a sample PUBLIC motion. Please vote.', 'public', 'regular', NULL), (2, 1, '{\"id\":232726,\"name\":\"Pim Oude Veldhuis\"}', 'preparation', 'This is a sample ANONYMOUS motion. Please vote.', 'anonymous', 'regular', NULL);");
            DB::statement("INSERT IGNORE INTO `users` (`id`, `status`, `access`, `name`) VALUES (101031, 'active', 'board', 'Eric'), (232726, 'active', 'member', 'Pim Oude Veldhuis'), (218981, 'active', 'board', 'Elias'), (276432, 'active', 'administrator', 'Bas Pigmans'), (345678, 'active', 'member', 'User 3'), (396868, 'active', 'administrator', 'Peter Bosch'), (456789, 'active', 'member', 'User 4');");
        }
        
        return redirect('/');
    }
}