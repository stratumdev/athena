<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@render');
Route::match(['get', 'post'], '/account', 'DashboardController@account_edit');

Route::get('/login', 'LoginController@render');
Route::get('/logout', 'LoginController@logout');

Route::get('/install', 'InstallController@render');
Route::get('/update', 'InstallController@render');

Route::match('get', '/admin/ajax/{action?}','Admin\AjaxController@render');
Route::match('get', '/ajax/{action?}','AjaxController@render');

Route::get('/admin', 'Admin\DashboardController@render');

Route::match(['get', 'post'], '/admin/user', 'Admin\UserController@render');
Route::match(['get', 'post'], '/admin/user/member/{user}', 'Admin\UserController@render');
Route::match(['get', 'post'], '/admin/user/board/{user}', 'Admin\UserController@render');
Route::match(['get', 'post'], '/admin/user/delete/{user}', 'Admin\UserController@render');

Route::match(['get', 'post'], '/admin/meeting/new', 'Admin\MeetingController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}', 'Admin\MeetingController@render');
Route::match(['get', 'post'], '/admin/meeting/delete/{meeting}', 'Admin\MeetingController@render');
Route::match(['get', 'post'], '/admin/meeting/open/{meeting}', 'Admin\MeetingController@render');
Route::match(['get', 'post'], '/admin/meeting/close/{meeting}', 'Admin\MeetingController@render');
Route::match(['get', 'post'], '/admin/meeting/publish/{meeting}', 'Admin\MeetingController@render');

Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/proxy', 'Admin\ProxyController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/proxy/delete/{user}', 'Admin\ProxyController@render');

Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/new', 'Admin\TopicController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/sort', 'Admin\TopicController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/edit/{topic}', 'Admin\TopicController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/delete/{topic}', 'Admin\TopicController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/open/{topic}', 'Admin\TopicController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/close/{topic}', 'Admin\TopicController@render');

Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/edit/{topic}/motion/new', 'Admin\MotionController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/edit/{topic}/motion/edit/{motion}', 'Admin\MotionController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/edit/{topic}/motion/delete/{motion}', 'Admin\MotionController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/edit/{topic}/motion/open/{motion}', 'Admin\MotionController@render');
Route::match(['get', 'post'], '/admin/meeting/edit/{meeting}/topic/edit/{topic}/motion/close/{motion}', 'Admin\MotionController@render');

Route::get('/report/{meeting}', 'ReportController@render');
Route::get('/report/pdf/{meeting}', 'ReportController@pdf');

Route::get('/meetings','MeetingController@render');
Route::get('/meeting/{meeting}','MeetingController@render');
Route::get('/meeting/{meeting}/topic/{topic}','MeetingController@render');
Route::match(['get', 'post'], '/meeting/{meeting}/topic/{topic}/motion/{motion}','MeetingController@render');
Route::post('/meeting/{meeting}/topic/{topic}/vote', 'MeetingController@renderMotions')->name('submit_multivote');