Dear NPO member,

The minutes from a recent meeting have just been published.

Please find a URL to the Meeting Minutes PDF below:
{{ $pdf_url }}

With Kindest Regards,

Bladiebla
IVAO NPO Secretary