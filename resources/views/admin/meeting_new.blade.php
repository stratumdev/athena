@extends('template')

@section('content')
    <form class="form-horizontal" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        @if(!empty($error))
            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <div class="alert alert-danger">
                        The following errors have occured:
                        
                        <ul>
                            @foreach($error AS $msg)
                                <li>{{ $msg }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Type</label>  
            <div class="col-md-4">
                <select name="type" class="form-control">
                    <option value="GA" {{ isset($input_type) ? ($input_type == 'GA' ? 'selected' : '') : '' }}>GA</option>
                    <option value="BoG" {{ isset($input_type) ? ($input_type == 'BoG' ? 'selected' : '') : '' }}>BoG</option>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Date</label>  
            <div class="col-md-4"><input name="date" type="text" class="form-control input-md" placeholder="2017-12-01" value="{{ $input_date or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Time start (UTC)</label>  
            <div class="col-md-4"><input name="start" type="text" class="form-control input-md" placeholder="18:00" value="{{ $input_start or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Time end (UTC)</label>  
            <div class="col-md-4"><input name="end" type="text" class="form-control input-md" placeholder="21:00" value="{{ $input_end or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Chairman VID</label>
            <div class="col-md-4"><input name="chairman" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_chairman or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Called by</label>  
            <div class="col-md-4"><input name="calledby" type="text" class="form-control input-md" placeholder="BoG" value="{{ $input_calledby or '' }}"></div>
        </div>

        <div class="form-group">
            <div class="col-md-4 col-md-offset-2">
                <button class="btn btn-success">Create Meeting</button> <a class="btn btn-danger" href="/admin">Cancel</a>
            </div>
        </div>
    </form>
@endsection