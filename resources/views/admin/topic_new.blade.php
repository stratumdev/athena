@extends('template')

@section('content')
    <form class="form-horizontal" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        @if(!empty($error))
            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <div class="alert alert-danger">
                        The following errors have occured:
                        
                        <ul>
                            @foreach($error AS $msg)
                                <li>{{ $msg }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Author VID</label>  
            <div class="col-md-4"><input name="author" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_author or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Title</label>  
            <div class="col-md-4"><input name="title" type="text" class="form-control input-md" value="{{ $input_title or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Description</label>  
            <div class="col-md-4"><textarea rows="7" class="form-control" name="description">{{ $input_description or '' }}</textarea></div>
        </div>

        <div class="form-group">
            <div class="col-md-4 col-md-offset-2">
                <button class="btn btn-success">Create Topic</button> <a class="btn btn-danger" href="/admin/meeting/edit/{{ $meeting->id }}">Cancel</a>
            </div>
        </div>
    </form>
@endsection