@extends('template')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <h2>NPO users</h2>
            @if(count($users) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-primary">
                            <th>VID</th>
                            <th>Name</th>
                            <th>Access</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users AS $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->access }}</td>
                                
                                @if($user->access == 'member')
                                    <td><a href="/admin/user/board/{{ $user->id }}">Make governor</a></td>
                                @elseif($user->access == 'board')
                                    <td><a href="/admin/user/member/{{ $user->id }}">Make member</a></td>
                                @elseif($user->access == 'administrator')
                                    <td></td>
                                @endif
                                
                                @if($user->access != 'administrator')
                                    <td><a href="/admin/user/delete/{{ $user->id }}">Delete</a></td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>

        <div class="col-md-6">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(!empty($error))
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="alert alert-danger">
                                The following errors have occured:

                                <ul>
                                    @foreach($error AS $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4"><h2>Add NPO user</h2></div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">VID</label>  
                    <div class="col-md-8"><input name="id" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_id or '' }}"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Name</label>  
                    <div class="col-md-8"><input name="name" type="text" class="form-control input-md" placeholder="Pim Oude Veldhuis" value="{{ $input_name or '' }}"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button class="btn btn-success">Create User</button> <a class="btn btn-danger" href="/admin/user">Cancel</a>
                    </div>
                </div>
            </form>
            <hr>
            
            @if(isset($administrators) && count($administrators) > 0)
                <div class="text-center">
                    <h2>Current administrators</h2>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-primary">
                            <th>VID</th><th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($administrators AS $administrator)
                            <tr>
                                <td>{{ $administrator->id }}</td><td>{{ $administrator->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection