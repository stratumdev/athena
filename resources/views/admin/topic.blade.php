@extends('template')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="text-center">
                <a class="btn btn-default" href="/admin/meeting/edit/{{ $meeting->id }}">Back</a>
                
                @if($meeting->status == 'preparation' || $meeting->status == 'open')
                    <a class="btn btn-success" href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}/motion/new">Create a new motion</a>
                @endif
                
                @if($meeting->status == 'open')
                    <a class="btn btn-primary" href="/admin/meeting/edit/{{ $meeting->id }}/topic/open/{{ $topic->id }}">Open all motions</a>
                    <a class="btn btn-primary" href="/admin/meeting/edit/{{ $meeting->id }}/topic/close/{{ $topic->id }}">Close opened motions</a>
                @endif
            </div><br />
            
            @if(count($motions) > 0)
            <table class="table table-striped">
                <thead>
                    <tr class="bg-primary">
                        <th>Description</th>
                        <th>Author</th>
                        <th>Type</th>
                        <th>Status</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($motions AS $motion)
                        <tr>
                            <td><a href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}/motion/edit/{{ $motion->id }}">{!! nl2br(e($motion->description)) !!}</a></td>
                            <td>{{ $motion->author->id }}</td>
                            <td>{{ $motion->type }}, {{ $motion->majority }}</td>
                            <td class="@if($motion->status == 'preparation') alert-danger @elseif($motion->status == 'open') alert-warning @else alert-success @endif">{{ $motion->status }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>

        <div class="col-md-5">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(!empty($error))
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="alert alert-danger">
                                The following errors have occured:

                                <ul>
                                    @foreach($error AS $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4"><h2>Topic settings</h2></div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Author VID</label>  
                    <div class="col-md-8"><input name="author" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_author or '' }}" @if($meeting->status == 'published')disabled="disabled"@endif></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Title</label>  
                    <div class="col-md-8"><input name="title" type="text" class="form-control input-md" value="{{ $input_title or '' }}" @if($meeting->status == 'published')disabled="disabled"@endif></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Description</label>  
                    <div class="col-md-8"><textarea rows="7" class="form-control" name="description" @if($meeting->status == 'published')disabled="disabled"@endif>{{ $input_description or '' }}</textarea></div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button class="btn btn-success">Save topic</button>
                        
                        @if(count($motions) == 0)
                            <a class="btn btn-danger" href="/admin/meeting/edit/{{ $meeting->id }}/topic/delete/{{ $topic->id }}" onclick="return confirm('Cannot undo deleting a topic, are you sure?')">Delete Topic</a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection