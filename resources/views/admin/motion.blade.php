@extends('template')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="text-center">
                <a class="btn btn-default" href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}">Back</a><br /><br />
                
                @if($meeting->status == 'open')
                    @if($motion->status == 'preparation')
                        <a class="btn btn-primary" href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}/motion/open/{{ $motion->id }}">Open Motion</a><br /><br />
                    @elseif($motion->status == 'open')
                        <a class="btn btn-primary" href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}/motion/close/{{ $motion->id }}">Close Motion</a><br /><br />
                        <a class="btn btn-success" href="/meeting/{{ $meeting->id }}/topic/{{ $topic->id }}/motion/{{ $motion->id }}">Go to vote page</a><br /><br />
                    @endif
                @endif
            </div>
            @if($motion->status == 'closed')
                <h2>Motion: {{ $motion->description }}</h2>
                <p>Type: <strong>{{ $motion->type }}</strong><br/>
                    Majority: <strong>{{ $motion->majority }}</strong></p>
            
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-primary">
                            <th colspan="2">Results</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td>{{ $result['agree'] }}x</td><td>Agree</td></tr>
                        <tr><td>{{ $result['disagree'] }}x</td><td>Disagree</td></tr>
                        <tr><td>{{ $result['abstain'] }}x</td><td>Abstain</td></tr>
                        <tr class="alert @if($result['outcome'] == 'agree') alert-success @elseif($result['outcome'] == 'tie') alert-warning @else alert-danger @endif">
                            <td colspan="2"><strong>Conclusion: @if($result['outcome'] == 'agree') Agree @elseif($result['outcome'] == 'tie') Tie @else Disagree @endif</strong></td>
                        </tr>
                    </tbody>
                </table>
            @endif
        </div>

        <div class="col-md-6">
            @if($motion->status == 'preparation')
                <form class="form-horizontal" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    @if(!empty($error))
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <div class="alert alert-danger">
                                    The following errors have occured:

                                    <ul>
                                        @foreach($error AS $msg)
                                            <li>{{ $msg }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4"><h2>Motion settings</h2></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Author VID</label>  
                        <div class="col-md-8"><input name="author" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_author or '' }}" @if($meeting->status == 'closed' || $meeting->status == 'published')disabled="disabled"@endif></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Description</label>  
                        <div class="col-md-8"><textarea rows="7" class="form-control" name="description" @if($meeting->status == 'closed' || $meeting->status == 'published')disabled="disabled"@endif>{{ $input_description or '' }}</textarea></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Type</label>  
                        <div class="col-md-8">
                            <select name="type" class="form-control" @if($meeting->status == 'closed' || $meeting->status == 'published')disabled="disabled"@endif>
                                <option value="public" {{ isset($input_type) ? ($input_type == 'public' ? 'selected' : '') : '' }}>Public</option>
                                <option value="anonymous" {{ isset($input_type) ? ($input_type == 'anonymous' ? 'selected' : '') : '' }}>Anonymous</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Majority</label>  
                        <div class="col-md-8">
                            <select name="majority" class="form-control" @if($meeting->status == 'closed' || $meeting->status == 'published')disabled="disabled"@endif>
                                <option value="regular" {{ isset($input_majority) ? ($input_majority == 'regular' ? 'selected' : '') : '' }}>Regular</option>
                                <option value="twothird" {{ isset($input_majority) ? ($input_majority == 'twothird' ? 'selected' : '') : '' }}>2/3rd</option>
                                <option value="fourfifth" {{ isset($input_majority) ? ($input_majority == 'fourfifth' ? 'selected' : '') : '' }}>4/5th</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            @if($meeting->status == 'preparation' || $meeting->status == 'open')
                                <button class="btn btn-success">Save Motion</button>
                            @endif
                            
                            @if(($meeting->status == 'preparation' || $meeting->status == 'open') && $motion->status == 'preparation')
                                <a class="btn btn-danger" href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}/motion/delete/{{ $motion->id }}" onclick="return confirm('Deleting cannot be undone, are you sure?')">Delete Motion</a>
                            @endif
                        </div>
                    </div>
                </form>
            @else            
                @if($motion->status == 'closed')                                
                    @if($motion->type == 'public' && count($result['votes']) > 0)
                        <table class="table table-striped">
                            <thead>
                                <tr class="bg-primary">
                                    <td colspan="2">List of votes</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Agree:</td>
                                    <td>
                                    @if(count($result['agree_votes']) > 0)
                                        {{ implode(', ', $result['agree_votes']) }}
                                    @else
                                        -
                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Disagree:</td>
                                    <td>
                                    @if(count($result['disagree_votes']) > 0)
                                        {{ implode(', ', $result['disagree_votes']) }}
                                    @else
                                        -
                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Abstain:</td>
                                    <td>
                                    @if(count($result['abstain_votes']) > 0)
                                        {{ implode(', ', $result['abstain_votes']) }}
                                    @else
                                        -
                                    @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info">No public votes were cast</div>
                    @endif
                @elseif($motion->status == 'open' && count($waitingfor) > 0)
                    <h2>Voting in progress</h2>
                    <table class="table table-striped">
                        <thead>
                            <tr class="bg-primary">
                                <th>
                                    Waiting for votes from:
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($waitingfor AS $user)
                            <tr>
                                <td>{{ $user->name or '' }} ({{ $user->user }})</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @elseif($motion->status == 'open' && count($waitingfor) == 0)
                    <h2>All votes have been cast</h2>
                @else
                    <p>Not voted.</p>
                @endif
            @endif
        </div>
    </div>
@endsection