@extends('template')

@section('content')
    <div class="text-center">
        <a class="btn btn-success" href="/admin/meeting/new">Create a new meeting</a>
        <a class="btn btn-primary" href="/admin/user">User management</a>
    </div>
    <br />

    @if(isset($meetings))
        <table class="table table-striped">
            <thead>
                <tr class="bg-primary">
                    <th>Type</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Chairman</th>
                    <th class="text-center">Called by</th>
                    <th class="text-center">Status</th>
                    <th></th>
                </tr>
            </thead>
            
            <tbody>
                @foreach($meetings AS $meeting)
                    <tr>
                        <td>{{ $meeting->type }}</td>
                        <td class="text-center">{{ date('d M Y H:i e', strtotime($meeting->start)) }}</td>
                        <td class="text-center">{{ $meeting->chairman->name }} ({{ $meeting->chairman->id }})</td>
                        <td class="text-center">{{ $meeting->calledby }}</td>
                        <td class="text-center @if($meeting->status == 'preparation') alert-danger @elseif($meeting->status == 'open') alert-warning @elseif($meeting->status == 'published') alert-info @else alert-success @endif">{{ $meeting->status }}</td>
                        <td class="text-right">
                            @if($meeting->status != 'published')
                                <a href="/admin/meeting/edit/{{ $meeting->id }}">Manage</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection