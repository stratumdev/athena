@extends('template')

@section('content')
    <div class="row">
        <div class="col-md-7">
            @if($meeting->status == 'preparation' || $meeting->status == 'open')
                <div class="text-center"><a class="btn btn-success" href="/admin/meeting/edit/{{ $meeting->id }}/topic/new">Create a new topic</a>
                @if($meeting->type == 'GA')
                    <a class="btn btn-primary" href="/admin/meeting/edit/{{ $meeting->id }}/proxy">Manage proxies</a>
                @endif
                @if($meeting->status == 'preparation')
                    <a class="btn btn-primary" href="/admin/meeting/open/{{ $meeting->id }}" onclick="return confirm('Opening cannot be undone, are you sure?')">Open meeting</a>
                @elseif($meeting->status == 'open')
                    <a class="btn btn-primary" href="/admin/meeting/close/{{ $meeting->id }}" onclick="return confirm('Closing cannot be undone, are you sure?')">Close meeting</a>
                @endif
                </div>
            @endif
            
            @if($meeting->status == 'closed')
                <div class="text-center">
                    <a class="btn btn-primary" href="/admin/meeting/publish/{{ $meeting->id }}" onclick="return confirm('Publishing cannot be undone, are you sure?')">Publish meeting</a>
                </div>
            @endif
            <hr/>
            @if(count($topics) > 0)
            <table class="table table-striped sortable">
                <thead>
                    <tr class="bg-primary">
                        <th>Title</th>
                        <th>Author</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($topics AS $topic)
                        <tr topic="{{ $topic->id }}">
                            <td><a href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}">{{ $topic->title }}</a></td>
                            <td>{{ $topic->author->name }} ({{ $topic->author->id }})</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>

        <div class="col-md-5">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(!empty($error))
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="alert alert-danger">
                                The following errors have occured:

                                <ul>
                                    @foreach($error AS $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4"><h2>Meeting settings</h2></div>
                </div>
                
                @if($meeting->status == 'preparation')
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Type</label>  
                        <div class="col-md-8">
                            <select name="type" class="form-control">
                                <option value="GA" {{ isset($input_type) ? ($input_type == 'GA' ? 'selected' : '') : '' }}>GA</option>
                                <option value="BoG" {{ isset($input_type) ? ($input_type == 'BoG' ? 'selected' : '') : '' }}>BoG</option>
                            </select>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Date</label>  
                    <div class="col-md-8"><input name="date" type="text" class="form-control input-md" placeholder="2017-12-01" value="{{ $input_date or '' }}"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Time start (UTC)</label>  
                    <div class="col-md-8"><input name="start" type="text" class="form-control input-md" placeholder="18:00" value="{{ $input_start or '' }}"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Time end (UTC)</label>  
                    <div class="col-md-8"><input name="end" type="text" class="form-control input-md" placeholder="21:00" value="{{ $input_end or '' }}"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Chairman VID</label>
                    <div class="col-md-8"><input name="chairman" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_chairman or '' }}"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Called by</label>  
                    <div class="col-md-8"><input name="calledby" type="text" class="form-control input-md" placeholder="BoG" value="{{ $input_calledby or '' }}"></div>
                </div>

                @if($meeting->status == 'closed')
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Meeting notes</label>  
                        <div class="col-md-8"><textarea rows="7" class="form-control" name="notes">{{ $input_notes or '' }}</textarea></div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button class="btn btn-success">Save changes</button>
                        
                        @if($meeting->status == 'preparation')
                            <a class="btn btn-danger" href="/admin/meeting/delete/{{ $meeting->id }}" onclick="return confirm('Deleting cannot be undone, are you sure?')">Delete Meeting</a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>


    <script type="text/javascript">
        var sortableTableHelper = function(e, ui) { ui.children().each(function() { $(this).width($(this).width()); }); return ui; };
        $(".sortable tbody").sortable({
            helper: sortableTableHelper,
            stop: function (event, ui) {
                var topic_order = [];
                $.each($(this).find('tr'), function (index, value) {
                    topic_order.push($(this).attr('topic'));
                });

                $.ajax({
                    method: "GET",
                    url: "/admin/meeting/edit/{{ $meeting->id }}/topic/sort",
                    data: { order: topic_order }
                });
            }
        }).disableSelection();
    </script>
@endsection