@extends('template')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="text-center"><a class="btn btn-default" href="/admin/meeting/edit/{{ $meeting->id }}">Back</a></div><br/>
            
            @if(count($proxies) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-primary">
                            <th>VID From</th>
                            <th>VID To</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($proxies AS $proxy)
                            <tr>
                                <td>{{ $proxy->user }}</td>
                                <td>{{ $proxy->proxy }}</td>
                                <td><a href="/admin/meeting/edit/{{ $meeting->id }}/proxy/delete/{{ $proxy->user }}">Delete</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>

        <div class="col-md-6">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(!empty($error))
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <div class="alert alert-danger">
                                The following errors have occured:

                                <ul>
                                    @foreach($error AS $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                        <div class="col-md-8 col-md-offset-4"><h2>Add proxy</h2></div>
                    </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">VID From</label>  
                    <div class="col-md-8"><input name="user" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_user or '' }}"></div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">VID To</label>  
                    <div class="col-md-8"><input name="proxy" type="text" class="form-control input-md" placeholder="396868" value="{{ $input_proxy or '' }}"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button class="btn btn-success">Create Proxy</button> <a class="btn btn-danger" href="/admin/meeting/edit/{{ $meeting->id }}/proxy">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection