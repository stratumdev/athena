@extends('template')

@section('content')
    <form class="form-horizontal" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        @if(!empty($error))
            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <div class="alert alert-danger">
                        The following errors have occured:
                        
                        <ul>
                            @foreach($error AS $msg)
                                <li>{{ $msg }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2"><h2>Add new motion</h2></div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Author VID</label>  
            <div class="col-md-4"><input name="author" type="text" class="form-control input-md" placeholder="232726" value="{{ $input_author or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Description</label>  
            <div class="col-md-4"><textarea rows="7" class="form-control" name="description">{{ $input_description or '' }}</textarea></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Type</label>  
            <div class="col-md-4">
                <select name="type" class="form-control">
                    <option value="public" {{ isset($input_type) ? ($input_type == 'public' ? 'selected' : '') : '' }}>Public</option>
                    <option value="anonymous" {{ isset($input_type) ? ($input_type == 'anonymous' ? 'selected' : '') : '' }}>Anonymous</option>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="textinput">Majority</label>  
            <div class="col-md-4">
                <select name="majority" class="form-control">
                    <option value="regular" {{ isset($input_majority) ? ($input_majority == 'regular' ? 'selected' : '') : '' }}>Regular</option>
                    <option value="twothird" {{ isset($input_majority) ? ($input_majority == 'twothird' ? 'selected' : '') : '' }}>2/3rd</option>
                    <option value="fourfifth" {{ isset($input_majority) ? ($input_majority == 'fourfifth' ? 'selected' : '') : '' }}>4/5th</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-4 col-md-offset-2">
                <button class="btn btn-success">Create Motion</button> <a class="btn btn-danger" href="/admin/meeting/edit/{{ $meeting->id }}/topic/edit/{{ $topic->id }}">Cancel</a>
            </div>
        </div>
    </form>
@endsection