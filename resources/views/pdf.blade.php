<style>
	body {
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	}
	hr {
		margin-top: 20px;
    	margin-bottom: 20px;
    	border: 0;
    	border-top: 1px solid #eee;
		box-sizing: content-box;
		height: 0;
	}
	h1, h2, h3 {
		font-family: Arial; 
		font-weight: 400; 
		line-heigth: 1.1; 
		color: #2a4982; 
		letter-spacing: 2px; 
		text-transform: uppercase;
	}
	h1 {
		font-size: 22px;
	}
	h2 {
		font-size: 18px;
	}
	.text-center {
		text-align: center;
	}
	table {
    	width: 100%;
    	max-width: 100%;
    	margin-bottom: 20px;
	}
	/*td {
    	line-height: 1.42857143;
    	vertical-align: top;
    	border-top: 1px solid #ddd;
	}*/
	.topic-title {
    	background-color: #f9f9f9;
	}
	th {
		border-top: 0;
		vertical-align: bottom;
    	border-bottom: 2px solid #ddd;
		line-height: 1.42857143;
		font-weight: normal;
		text-align: left;
	}
	
	
</style>
<h1 class="text-center">@if($meeting->type == 'GA') General Assembly @else BoG @endif meeting of {{ date('d M Y', strtotime($meeting->start)) }}</h1>
<p class="text-center">Chairman: {{ $meeting->chairman->name }} ({{ $meeting->chairman->id }}), status: {{ $meeting->status }}
    <br/>Meeting opened: {{ date('H:i e', strtotime($meeting->start)) }}
    <br/>Meeting closed: {{ date('H:i e', strtotime($meeting->end)) }}
</p>

<hr />

@if($meeting->status == 'published' && !empty($meeting->notes))
    <h2 class="text-center">Meeting notes</h2>
    <p>{!! nl2br(e($meeting->notes)) !!}</p>
    <hr />
@endif

@if(count($topics) > 0)
    <h2 class="text-center">Topics</h2>
    <table class="table table-striped">
        <tbody>
            @foreach($topics AS $topic)
                <tr class="topic-title">
                    <td style="padding: 8px;">{{ $topic->title }}<br /><small>{{ $topic->description }}</small></td>
                </tr>
                <tr>
                    <td>
                        @if(isset($motions[$topic->id]))
                            <table class="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th colspan="3" style="padding: 8px; color: #fff; background-color: #337ab7;">Motions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($motions[$topic->id] AS $motion)
                                        <tr>
                                            <td style="padding: 5px; width:100%;" colspan="2"><strong><small>{!! nl2br(e($motion['motion']->description)) !!}</small></strong></td>
										</tr>
										<tr>
											<td style="padding:5px;"><small><i>Author: {{ $motion['motion']->author->name }} ({{ $motion['motion']->author->id }})</i></small></td>

                                            @if(isset($motion['result']))
                                                <td style="padding: 5px; text-align:right;"><small>Agree: {{ $motion['result']['agree'] }}x; Disagree: {{ $motion['result']['disagree'] }}x; Abstain: {{ $motion['result']['abstain'] }}x; <strong>Conclusion: @if($motion['result']['outcome'] == 'agree') Agree @elseif($motion['result']['outcome'] == 'tie') Tie @else Disagree @endif</strong></small></td>
                                            @else
                                                <td style="padding: 5px; text-align:right;"><small><strong>Conclusion: none</strong></small></td>
                                            @endif
                                        </tr>

                                        @if(isset($motion['result']))
                                            <tr>
                                                <td colspan="2" style="padding: 5px; padding-top:0px; border-bottom: 1px solid #eee; text-align:left;"><small><br/>
                                                        @if(isset($motion['result']['votes']) && count($motion['result']['votes']) > 0)
                                                            @if(count($motion['result']['agree_votes']) > 0)
                                                                Agree: {{ implode(', ', $motion['result']['agree_votes']) }}
                                                            @else
                                                                Agree: <i>none</i>
                                                            @endif
                                                            <br/>
                                                            @if(count($motion['result']['disagree_votes']) > 0)
                                                                Disagree: {{ implode(', ', $motion['result']['disagree_votes']) }}
                                                            @else
                                                                Disagree: <i>none</i>
                                                            @endif
                                                            <br/>
                                                            @if(count($motion['result']['abstain_votes']) > 0)
                                                                Abstain: {{ implode(', ', $motion['result']['abstain_votes']) }}
                                                            @else
                                                                Abstain: <i>none</i>
                                                            @endif
                                                        @elseif($motion['motion']->type == 'anonymous')
                                                            <i>Votes were cast anonymously.</i>
                                                        @else
                                                            <i>No votes recorded for this motion.</i>
                                                        @endif
                                                    </small>
                                                </td>
                                            </tr>
                                        @else
                                            <tr><td colspan="3"><small><i>Motion was not voted on</i></small></td></tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <i><small>No motions in this topic</small></i>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <hr/>
@endif

<h2 class="text-center">Attendance</h2>
<table class="table table-striped" cellspacing="0" style="width:100%">
    <thead>
        <tr class="topic-title" style="background-color: #337ab7; color: #fff;">
            <th style="padding: 8px; width:20%; text-align:center;"># Invited</th>
            <th style="padding: 8px; width:20%; text-align:center;"># Present</th>
            <th style="padding: 8px; width:20%; text-align:center;"># Proxied</th>
            <th style="padding: 8px; width:20%; text-align:center;"># Absent</th>
            <th style="padding: 8px; width:20%; text-align:center;">Meeting status</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="padding: 3px; width:20%; text-align:center;">{{ $count_invited }}</td>
            <td style="padding: 3px; width:20%; text-align:center;">{{ $count_present }}</td>
            <td style="padding: 3px; width:20%; text-align:center;">{{ $count_proxied }}</td>
            <td style="padding: 3px; width:20%; text-align:center;">{{ $count_absent }}</td>
            <td style="padding: 3px; width:20%; text-align:center;">{{ $meeting_status }}</td>
        </tr>
    </tbody>
</table>
<p></p>
<table class="table table-striped" cellspacing="0" style="width:100%">
    <thead>
        <tr class="topic-title" style="background-color: #337ab7; color: #fff;">
            <th colspan="2" style="padding: 8px; width:50%;">Participant</th>
            <th style="padding: 8px; width:25%; text-align:center;">Status</th>
            <th style="padding: 8px; width:25%; text-align:center;">Proxy</th>
        </tr>
    </thead>

    <tbody>
        @if(count($users) > 0)
            @foreach($users AS $user)
                <tr>
                    <td width="5%" style="padding:3px;">{{ $user['id'] }}</td>
                    <td width="30%" style="padding:3px;">{{ $user['name'] }}</td>

                    @if($user['status'] == 'present')
                        <td class="text-center" style="color: rgb(70, 136, 71); background-color: rgb(223, 240, 216); padding:3px;">present</td>
                    @elseif($user['status'] == 'proxy-absent')
                        <td class="text-center" style="color: #a94442; background-color: #f2dede; padding:3px;">proxy absent</td>
                    @elseif($user['status'] == 'proxied')
                        <td class="text-center" style="color: rgb(70, 136, 71); background-color: rgb(223, 240, 216); padding:3px;">proxied</td>
                    @else
                        <td class="text-center" style="color: #a94442; background-color: #f2dede; padding:3px;">absent</td>
                    @endif

                    @if($user['status'] == 'proxied' || $user['status'] == 'proxy-absent')
                        <td width="40%" style="padding:3px;">{{ $user['proxy'] }}</td>
                    @else
                        <td width="40%" style="padding:3px;"></td>
                    @endif
                </tr>
            @endforeach
        @endif
    </tbody>
</table>