<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>iVote 2.0 for IVAO</title>

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>

        <!-- Bootstrap core CSS -->
        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/ivao.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    </head>

    <body>
        <div class="container">
            <div class="header clearfix">
            <nav>
                <ul class="nav nav-pills pull-right">
                    <li role="presentation"><a href="/">Home</a></li>
                    <li role="presentation"><a href="/account">Account</a></li>
                    
                    @if($isAdmin == true)
                        <li role="presentation"><a href="/admin">Administrator</a></li>
                    @endif
                    
                    <li role="presentation"><a href="/logout">Logout</a></li>
                </ul>
            </nav>
                <a href="/" id="brand-header"><img id="logo-header" src="https://ivao.aero/assets/img/logo1-default.png"/>
                iVote 2.0</a>
            </div>

            <div class="row marketing">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>

            <footer class="footer">
                <p>&copy; 2017-{{ date('Y') }} International Virtual Aviation Organisation. All Rights Reserved.</p>
                <p class="small">Created for IVAO by Pim Oude Veldhuis (<a href="https://www.ivao.aero/Member.aspx?Id=232726" target="_blank">232726</a>), Bas Pigmans (<a href="https://www.ivao.aero/Member.aspx?Id=276432" target="_blank">276432</a>) and Peter Bosch (<a href="https://www.ivao.aero/Member.aspx?Id=396868" target="_blank">396868</a>)</p>
            </footer>
        </div>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/js/javascript.js"></script>
    </body>
</html>

