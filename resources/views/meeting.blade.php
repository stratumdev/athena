@extends('template')

@section('content')
    <h1 class="text-center">@if($meeting->type == 'GA') General Assembly @else BoG @endif meeting of {{ date('d M Y H:i e', strtotime($meeting->start)) }}</h1>
    <p class="text-center">Chairman: {{ $meeting->chairman->name }} ({{ $meeting->chairman->id }}), status: {{ $meeting->status }}</p>

    <hr />

    @if(count($topics) > 0)
        <h2 class="text-center">Topics</h2>
        <table class="table table-striped">
            <tbody>
                @foreach($topics AS $topic)
                    <tr>
                        <td><a href="/meeting/{{ $meeting->id }}/topic/{{ $topic->id }}">{{ $topic->title }}</a><br /><small>{{ $topic->description }}</small></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <hr/>
    @endif

    <h2 class="text-center">Attendance</h2>
    <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th class="text-center"># Invited</th>
                <th class="text-center"># Present</th>
                <th class="text-center"># Proxied</th>
                <th class="text-center"># Absent</th>
                <th class="text-center">Meeting status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">{{ $count_invited }}</td>
                <td class="text-center">{{ $count_present }}</td>
                <td class="text-center">{{ $count_proxied }}</td>
                <td class="text-center">{{ $count_absent }}</td>
                <td class="text-center">{{ $meeting_status }}</td>
            </tr>
        </tbody>
    </table>

    <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th colspan="2">Participant</th>
                <th class="text-center">Status</th>
                <th>Proxy</th>
            </tr>
        </thead>
        
        <tbody>
            @if(count($users) > 0)
                @foreach($users AS $user)
                    <tr>
                        <td width="10%">{{ $user['id'] }}</td>
                        <td width="30%">{{ $user['name'] }}</td>
                        
                        @if($user['status'] == 'present')
                            <td class="text-center alert-success">present</td>
                        @elseif($user['status'] == 'proxied')
                            <td class="text-center alert-info">proxied</td>
                        @elseif($user['status'] == 'proxy-invited')
                            <td class="text-center alert-warning">proxy invited</td>
                        @else
                            <td class="text-center">invited</td>
                        @endif
                        
                        @if($user['status'] == 'proxied' || $user['status'] == 'proxy-invited')
                            <td width="40%">{{ $user['proxy'] }}</td>
                        @else
                            <td width="40%"></td>
                        @endif
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@endsection