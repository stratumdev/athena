@extends('template')

@section('content')
    <h1 class="text-center">Account edit</h1>

    <hr />

    <h2>Notification email</h2>
	<p>Enter your email address to receive a notification when a meeting has been published. If you would no longer like to receive an emails from this system, just delete your email address and click "Update e-mail"</p>
    <form method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        @if(isset($error))
            <div class="alert alert-danger">{{ $error }}</div>
        @endif
        
        <div class="form-group">
            <div class="col-md-4"><input id="input_email" name="email" type="text" class="form-control input-md" placeholder="E-mail address" value="{{ $input['email'] or '' }}"></div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                <button class="btn btn-success">Update e-mail</button>
            </div>
        </div>
    </form>
@endsection