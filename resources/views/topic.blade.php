@extends('template')

@section('content')
    <h1 class="text-center"><a href="/meeting/{{ $meeting->id }}">@if($meeting->type == 'GA') General Assembly @else BoG @endif meeting of {{ date('d M Y H:i e', strtotime($meeting->start)) }}</a></h1>
    <p class="text-center">Chairman: {{ $meeting->chairman->name }} ({{ $meeting->chairman->id }}), status: {{ $meeting->status }}</p>

    <h2>{{ $topic->title }}</h2>
    <p>{!! nl2br(e($topic->description)) !!}</p>

    <hr />
	@php( $show_submit_button = false )
    @if(count($motions))
        <h2>@if(count($motions) > 1) Motions @else Motion @endif</h2>
		<form method="post" action="{{ route('submit_multivote', ['topic' => $topic->id, 'meeting' => $meeting->id]) }}">
        <table class="table trable-striped">
            <thead>
                <tr class="bg-primary">
                    <th>Description</th>
                    <th>Status</th>
                    <th width="30%">Result</th>
                </tr>
            </thead>

            <tbody>
                @foreach($motions AS $motion)
                    <tr>
                        @if($motion['status'] == 'open' && $motion['did_i_vote'] == false)
                            <td>{{ $motion['description'] }}</td>
                        @else
                            <td>{{ $motion['description'] }}</td>
                        @endif
                        
                        @if($motion['status'] == 'preparation')
                            <td>Waiting to open</td>
                        @elseif($motion['status'] == 'open')
                            <td>Open for voting @if($motion['did_i_vote'] == true) (vote registered) @endif</td>
                        @else
                            <td>Closed</td>
                        @endif
                        
                        @if($motion['status'] == 'preparation')
                            <td class="alert-info">Waiting for motion to open</td>
                        @elseif($motion['status'] == 'open')
                            <td class="alert-info">Voting in progress</td>
                        @elseif($motion['status'] == 'closed')
                            @if($motion['outcome'] == 'agree')
                                <td class="alert-success">Agree</td>
                            @elseif($motion['outcome'] == 'tie')
                                <td class="alert-warning">Tie</td>
                            @else
                                <td class="alert-danger">Disagree</td>
                            @endif
                        @endif
                    </tr>
					@if($motion['status'] == 'closed' && $motion['result'] !== null)
						<tr>
							<td colspan="3" style="border-top: 0px;">
								<small>
									<strong>Agree ({{ count($motion['result']['agree_votes']) }}): </strong>
									@if(count($motion['result']['agree_votes']) > 0)
										{{ implode(', ', $motion['result']['agree_votes']) }}
									@else
										-
									@endif
									<br/>
									<strong>Disagree ({{ count($motion['result']['disagree_votes']) }}): </strong>
									@if(count($motion['result']['disagree_votes']) > 0)
										{{ implode(', ', $motion['result']['disagree_votes']) }}
									@else
										-
									@endif
									<br/>
									<strong>Abstain ({{ count($motion['result']['abstain_votes']) }}): </strong>
									@if(count($motion['result']['abstain_votes']) > 0)
										{{ implode(', ', $motion['result']['abstain_votes']) }}
									@else
										-
									@endif
								</small>
							</td>
						</tr>
					@elseif($motion['status'] == 'closed' && $motion['result'] === null)
						<tr>
							<td colspan="3" style="border-top: 0px;">
								<small>Votes were cast anonymously</small>
							</td>
						</tr>
					@elseif($motion['status'] == 'open' && $motion['waiting_for'] !== null)
						<tr>
							<td colspan="1" style="border-top: 0px;">
								<small>Currently waiting for {{ $motion['waiting_for'] }} vote(s) to be cast</small>
							</td>
							@if($motion['did_i_vote'] === false || $motion['need_i_vote_proxy'] === true)
								@php($show_submit_button = true)
								<td colspan="2" class="alert-warning" style="border-top: 0px;">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<table class="table table-condensed table-striped" style="margin-bottom:0px;">
										<tbody>
											@if($motion['did_i_vote'] === false)
											<tr>
												<td style="border-top:0px; width:50%">Your vote:</td>
												<td style="border-top:0px;"><input name="self[{{ $motion['id'] }}]" value="agree" type="radio" id="{{ $motion['id'] }}-yva"> <label for="{{ $motion['id'] }}-yva"><strong>Agree</strong></label></td>
												<td style="border-top:0px;"><input name="self[{{ $motion['id'] }}]" value="disagree" type="radio" id="{{ $motion['id'] }}-yvd"> <label for="{{ $motion['id'] }}-yvd"><strong>Disagree</strong></label></td>
												<td style="border-top:0px;"><input name="self[{{ $motion['id'] }}]" value="abstain" type="radio" id="{{ $motion['id'] }}-yvb"> <label for="{{ $motion['id'] }}-yvb"><strong>Abstain</strong></label></td>
											</tr>
											@endif
											@if($motion['need_i_vote_proxy'] === true)
											<tr>
												<td style="border-top:0px; width:50%">{{ $motion['proxy']->name }}'s vote:</td>
												<td style="border-top:0px;"><input name="proxy[{{ $motion['id'] }}]" value="agree" type="radio" id="{{ $motion['id'] }}-pva"> <label for="{{ $motion['id'] }}-pva"><strong>Agree</strong></label></td>
												<td style="border-top:0px;"><input name="proxy[{{ $motion['id'] }}]" value="disagree" type="radio" id="{{ $motion['id'] }}-pvd"> <label for="{{ $motion['id'] }}-pvd"><strong>Disagree</strong></label></td>
												<td style="border-top:0px;"><input name="proxy[{{ $motion['id'] }}]" value="abstain" type="radio" id="{{ $motion['id'] }}-pvb"> <label for="{{ $motion['id'] }}-pvb"><strong>Abstain</strong></label></td>
											</tr>
											@endif
										</tbody>
									</table>
								</td>
							@else
								<td colspan="2" class="alert-success" style="border-top: 0px;">
									<small>You have already voted on this motion</small>
								</td>
							@endif
						</tr>
					@elseif($motion['status'] == 'open' && $motion['waiting_for'] === null)
						<tr>
							<td colspan="3" style="border-top: 0px;">
								<small>All votes have been cast. Please wait for the chairman to close the motion.</small>
							</td>
						</tr>
					@endif
                @endforeach
            </tbody>
        </table>
		@if($show_submit_button)
			<button type="submit" class="btn btn-primary pull-right">SUBMIT VOTES</button>
		@endif
		</form>
    @endif
@endsection