@extends('template')

@section('content')
    <h1 class="text-center"><a href="/meeting/{{ $meeting->id }}">@if($meeting->type == 'GA') General Assembly @else BoG @endif meeting of {{ date('d M Y H:i e', strtotime($meeting->start)) }}</a></h1>
    <p class="text-center">Chairman: {{ $meeting->chairman->name }} ({{ $meeting->chairman->id }}), status: {{ $meeting->status }}</p>

    <h2>Motion</h2>
    <p><small>Author: {{ $motion->author->name }} ({{ $motion->author->id }}), type: {{ $motion->type }}, majority: {{ $motion->majority }}</small></p>
    <p>{!! nl2br(e($motion->description)) !!}</p>

    <hr />

    @if(!empty($error))
        <div class="alert alert-danger">
            The following errors have occured:

            <ul>
                @foreach($error AS $msg)
                    <li>{{ $msg }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td width="20%">Your vote</td>
                    <td>
                        <input id="self_agree" name="self" value="agree" type="radio" {{ isset($input_self) ? ($input_self == 'agree' ? 'checked' : '') : '' }} /> <label for="self_agree">Agree</label><br />
                        <input id="self_disagree" name="self" value="disagree" type="radio" {{ isset($input_self) ? ($input_self == 'disagree' ? 'checked' : '') : '' }} /> <label for="self_disagree">Disagree</label><br />
                        <input id="self_abstain" name="self" value="abstain" type="radio" {{ isset($input_self) ? ($input_self == 'abstain' ? 'checked' : '') : '' }} /> <label for="self_abstain">Abstain</label>
                    </td>
                </tr>

                @if(isset($proxy))
                    <tr>
                        <td width="20%">{{ $proxy->name }}'s vote</td>
                        <td>
                            <input id="proxy_agree" name="proxy" value="agree" type="radio" {{ isset($input_proxy) ? ($input_proxy == 'agree' ? 'checked' : '') : '' }} /> <label for="proxy_agree">Agree</label><br />
                            <input id="proxy_disagree" name="proxy" value="disagree" type="radio" {{ isset($input_proxy) ? ($input_proxy == 'disagree' ? 'checked' : '') : '' }} /> <label for="proxy_disagree">Disagree</label><br />
                            <input id="proxy_abstain" name="proxy" value="abstain" type="radio" {{ isset($input_proxy) ? ($input_proxy == 'abstain' ? 'checked' : '') : '' }} /> <label for="proxy_abstain">Abstain</label>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>

        <div class="text-center"><button class="btn btn-success">Confirm vote</button></div>
    </form>
@endsection