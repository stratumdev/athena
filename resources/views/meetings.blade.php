@extends('template')

@section('content')
    @if(count($active_meetings) > 0)
        <div class="alert alert-success">Active {{ $active_meetings[0]->type }} meeting in progress, <a class="alert-link" href="/meeting/{{ $active_meetings[0]->id }}">click here to go to the meeting</a>.</div>
    @endif

    <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th>Type</th>
                <th class="text-center">Date</th>
                <th class="text-center">Chairman</th>
                <th class="text-center">Called by</th>
                <th class="text-center">Status</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            @foreach($meetings AS $meeting)
                <tr>
                    <td>{{ $meeting->type }}</td>
                    <td class="text-center">{{ date('d M Y H:i e', strtotime($meeting->start)) }}</td>
                    <td class="text-center">{{ $meeting->chairman->name }} ({{ $meeting->chairman->id }})</td>
                    <td class="text-center">{{ $meeting->calledby }}</td>
                    <td class="text-center">{{ $meeting->status }}</td>
                    <td class="text-right">
                        @if($meeting->status == 'published')<a href="/report/{{ $meeting->id }}">Report</a> - <a href="/report/pdf/{{ $meeting->id }}" target="_blank">PDF</a>@endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection