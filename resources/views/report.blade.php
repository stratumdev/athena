@extends('template')

@section('content')
    <h1 class="text-center">@if($meeting->type == 'GA') General Assembly @else BoG @endif meeting of {{ date('d M Y', strtotime($meeting->start)) }}</h1>
    <p class="text-center">Chairman: {{ $meeting->chairman->name }} ({{ $meeting->chairman->id }}), status: {{ $meeting->status }}
        <br/>Meeting opened: {{ date('H:i e', strtotime($meeting->start)) }}
        <br/>Meeting closed: {{ date('H:i e', strtotime($meeting->end)) }}
    </p>

    <hr />

    @if($meeting->status == 'published' && !empty($meeting->notes))
        <h2 class="text-center">Meeting notes</h2>
        <p>{!! nl2br(e($meeting->notes)) !!}</p>
        <hr />
    @endif

    @if(count($topics) > 0)
        <h2 class="text-center">Topics</h2>
        <table class="table table-striped">
            <tbody>
                @foreach($topics AS $topic)
                    <tr>
                        <td>{{ $topic->title }}<br /><small>{{ $topic->description }}</small></td>
                    </tr>
                    <tr>
                        <td>
                            @if(isset($motions[$topic->id]))
                                <table class="table">
                                    <thead>
                                        <tr class="bg-primary">
                                            <th colspan="3">Motions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($motions[$topic->id] AS $motion)
                                            <tr>
                                                <td><strong><small>{!! nl2br(e($motion['motion']->description)) !!}</small></strong></td>
                                                <td><small><i>Author: {{ $motion['motion']->author->name }} ({{ $motion['motion']->author->id }})</i></small></td>
                                                
                                                @if(isset($motion['result']))
                                                    <td class="text-right"><small>Agree: {{ $motion['result']['agree'] }}x; Disagree: {{ $motion['result']['disagree'] }}x; Abstain: {{ $motion['result']['abstain'] }}x; <strong>Conclusion: @if($motion['result']['outcome'] == 'agree') Agree @elseif($motion['result']['outcome'] == 'tie') Tie @else Disagree @endif</strong></small></td>
                                                @else
                                                    <td class="text-right"><small><strong>Conclusion: none</strong></small></td>
                                                @endif
                                            </tr>
                                        
                                            @if(isset($motion['result']))
                                                <tr>
                                                    <td colspan="3">
                                                        <small>
                                                            @if(isset($motion['result']['votes']) && count($motion['result']['votes']) > 0)
                                                                @if(count($motion['result']['agree_votes']) > 0)
                                                                    Agree: {{ implode(', ', $motion['result']['agree_votes']) }}
                                                                @else
                                                                    Agree: <i>none</i>
                                                                @endif
                                                                <br/>
                                                                @if(count($motion['result']['disagree_votes']) > 0)
                                                                    Disagree: {{ implode(', ', $motion['result']['disagree_votes']) }}
                                                                @else
                                                                    Disagree: <i>none</i>
                                                                @endif
                                                                <br/>
                                                                @if(count($motion['result']['abstain_votes']) > 0)
                                                                    Abstain: {{ implode(', ', $motion['result']['abstain_votes']) }}
                                                                @else
                                                                    Abstain: <i>none</i>
                                                                @endif
                                                            @elseif($motion['motion']->type == 'anonymous')
                                                                <i>Votes were cast anonymously.</i>
                                                            @else
                                                                <i>No votes recorded for this motion.</i>
                                                            @endif
                                                        </small>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr><td colspan="3"><small><i>Motion was not voted on</i></small></td></tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <i><small>No motions in this topic</small></i>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <hr/>
    @endif

    <h2 class="text-center">Attendance</h2>
    <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th class="text-center"># Invited</th>
                <th class="text-center"># Present</th>
                <th class="text-center"># Proxied</th>
                <th class="text-center"># Absent</th>
                <th class="text-center">Meeting status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">{{ $count_invited }}</td>
                <td class="text-center">{{ $count_present }}</td>
                <td class="text-center">{{ $count_proxied }}</td>
                <td class="text-center">{{ $count_absent }}</td>
                <td class="text-center">{{ $meeting_status }}</td>
            </tr>
        </tbody>
    </table>

    <table class="table table-striped">
        <thead>
            <tr class="bg-primary">
                <th colspan="2">Participant</th>
                <th class="text-center">Status</th>
                <th>Proxy</th>
            </tr>
        </thead>
        
        <tbody>
            @if(count($users) > 0)
                @foreach($users AS $user)
                    <tr>
                        <td width="10%">{{ $user['id'] }}</td>
                        <td width="30%">{{ $user['name'] }}</td>
                        
                        @if($user['status'] == 'present')
                            <td class="text-center alert-success">present</td>
                        @elseif($user['status'] == 'proxy-absent')
                            <td class="text-center alert-danger">proxy absent</td>
                        @elseif($user['status'] == 'proxied')
                            <td class="text-center alert-success">proxied</td>
                        @else
                            <td class="text-center alert-danger">absent</td>
                        @endif
                        
                        @if($user['status'] == 'proxied' || $user['status'] == 'proxy-absent')
                            <td width="40%">{{ $user['proxy'] }}</td>
                        @else
                            <td width="40%"></td>
                        @endif
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@endsection