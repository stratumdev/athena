@extends('template')

@section('content')
    <h1 class="text-center"><a href="/meeting/{{ $meeting->id }}">@if($meeting->type == 'GA') General Assembly @else BoG @endif meeting of {{ date('d M Y H:i e', strtotime($meeting->start)) }}</a></h1>
    <p class="text-center">Chairman: {{ $meeting->chairman->name }} ({{ $meeting->chairman->id }}), status: {{ $meeting->status }}</p>

    <h2>Motion</h2>
    <p><small>Author: {{ $motion->author->name }} ({{ $motion->author->id }}), type: {{ $motion->type }}, majority: {{ $motion->majority }}</small></p>
    <p>{!! nl2br(e($motion->description)) !!}</p>

    <hr />
    
    <p class="text-center">We have successfully registered your vote(s).</p>
    <br /><br />
    <p class="text-center"><a class="btn btn-primary" href="/meeting/{{ $meeting->id }}/topic/{{ $topic->id }}">Click here to continue</a></p>
@endsection